﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CutoffCamera : MonoBehaviour
{
    Camera cutoffCamera;
    string globalCutoffTextureName = "_GlobalCutoffTex";

    void generateCutoffRenderTexture()
    {
        cutoffCamera = GetComponent<Camera> ();

        if (cutoffCamera.targetTexture != null)
        {
            RenderTexture temp = cutoffCamera.targetTexture;
            cutoffCamera.targetTexture = null;
            DestroyImmediate (temp);
        }

        cutoffCamera.targetTexture = new RenderTexture (cutoffCamera.pixelWidth, cutoffCamera.pixelHeight, 16);
        cutoffCamera.targetTexture.filterMode = FilterMode.Bilinear;

        Shader.SetGlobalTexture (globalCutoffTextureName, cutoffCamera.targetTexture);
    }

    private void OnEnable()
    {
        generateCutoffRenderTexture ();
    }
}
