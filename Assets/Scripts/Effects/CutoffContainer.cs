﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CutoffContainer : MonoBehaviour
{
    public delegate void CutoffContainerEventHandler(CutoffContainer cutoffContainer);
    public event CutoffContainerEventHandler OnCutoffContainerRefresh;

    public RectTransform RectTransform
    {
        get { return (RectTransform)this.transform; }
    }

	void Awake ()
    {
        deleteAll ();
	}

    public void Refresh ()
    {
        deleteAll ();

        if (OnCutoffContainerRefresh != null)
        {
            OnCutoffContainerRefresh (this);
        }
    }

    void deleteAll ()
    {
        for (int i = transform.childCount - 1; i >= 0; i--)
        {
            if (Application.isEditor)
            {
                DestroyImmediate (transform.GetChild (i).gameObject);
            }
            else
            {
                Destroy (transform.GetChild (i).gameObject);
            }
        }
    }
}
