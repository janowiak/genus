﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailParticle : MonoBehaviour
{
    public delegate void TrailParticleEventHandler(TrailParticle trailParticle);
    public event TrailParticleEventHandler OnTrailParticleFinished;

    protected NodeView target;
    bool running = false;

    const float MIN_THRESHOLD = 0.5f;
    const float SPEED = 20;

    public bool Running
    {
        get { return running; }
    }

    public void Run (Transform startTransform, NodeView target)
    {
        if (target != null)
        {
            this.transform.position = startTransform.position;
            this.gameObject.SetActive (true);
            this.target = target;
            running = true;
        }
    }

    private void Update()
    {
        if (running)
        {
            updatePosisition ();
            checkIfTargetReached ();

            if (! running)
            {
                disable ();
            }
        }
    }

    void disable ()
    {
        target.Increase ();
        running = false;
        StartCoroutine (waitAndDisable ());

        if (OnTrailParticleFinished != null)
        {
            OnTrailParticleFinished (this);
        }
    }

    IEnumerator waitAndDisable ()
    {
        yield return new WaitForSeconds (1f);
        this.gameObject.SetActive (false);
    }

    void updatePosisition ()
    {
        Vector3 direction = target.transform.position - this.transform.position;
        direction.Normalize ();
        direction.Scale (Vector3.one * SPEED * Time.deltaTime);

        this.transform.position += direction;
    }

    void checkIfTargetReached ()
    {
        if (target != null)
        {
            float distance = Vector3.Distance (target.transform.position, this.transform.position);

            if (distance <= MIN_THRESHOLD)
            {
                running = false;
            }
        }
    }
}
