﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ParticleManager : MonoBehaviour
{
    public delegate void ParticleManagerEventHandler();
    public event ParticleManagerEventHandler OnAllTrailParticlesFinished;

    const int PARTRICLES_COUNT = 20;

    [SerializeField] GameObject particlePrefab;
    [SerializeField] GameObject explosionPrefab;

    Queue<TrailParticle> trailParticles = new Queue<TrailParticle> ();
    Queue<ExplosionParticle> explosionParticles = new Queue<ExplosionParticle> ();

    void initParticleList ()
    {
        for (int i = 0; i < PARTRICLES_COUNT; i ++)
        {
            GameObject newParticleGameObject = Instantiate (particlePrefab);
            newParticleGameObject.transform.SetParent (this.transform);
            newParticleGameObject.SetActive (false);
            TrailParticle trailParticle = newParticleGameObject.GetComponent<TrailParticle> ();
            trailParticles.Enqueue (trailParticle);
            trailParticle.OnTrailParticleFinished += onTrailFinished;

            newParticleGameObject = Instantiate (explosionPrefab);
            newParticleGameObject.transform.SetParent (this.transform);
            newParticleGameObject.SetActive (false);
            ExplosionParticle explosionParticle = newParticleGameObject.GetComponent<ExplosionParticle> ();
            explosionParticles.Enqueue (explosionParticle);
        }
    }

    private void Awake()
    {
        initParticleList ();
    }

    public void RunNewParticle (NodeView startNodeView, NodeView target)
    {
        if (startNodeView != null && target != null && trailParticles != null && trailParticles.Count > 0)
        {
            TrailParticle trailParticle = trailParticles.Dequeue ();
            trailParticle.Run (startNodeView.transform, target);
            trailParticles.Enqueue (trailParticle);
        }
    }

    void runNewExplosionParticle (TrailParticle trailParticle)
    {
        ExplosionParticle explosionParticle = explosionParticles.Dequeue ();
        explosionParticle.transform.position = trailParticle.transform.position;
        explosionParticle.gameObject.SetActive (false);
        explosionParticle.gameObject.SetActive (true);
        explosionParticles.Enqueue (explosionParticle);
    }

    void onTrailFinished (TrailParticle trailParticle)
    {
        runNewExplosionParticle (trailParticle);

        bool allFinished = true;
        List<TrailParticle> trailParticlesList = this.trailParticles.ToList ();

        for (int i = 0; i < trailParticlesList.Count; i ++)
        {
            if (trailParticlesList [i].Running)
            {
                allFinished = false;

                break;
            }
        }

        if (allFinished)
        {
            if (OnAllTrailParticlesFinished != null)
            {
                OnAllTrailParticlesFinished ();
            }
        }
    }
}
