﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlurEffect : PostProcessingEffect
{
    public bool Active = false;
    public float Intensity;

    float [] _blurArray = new float [] {1/273f, 4/273f, 7/273f, 4/273f, 1/273f,
            4/273f, 16/273f, 26/273f, 16/273f, 4/273f,
             7/273f, 26/273f, 41/273f, 26/273f, 7/273f,
              4/273f, 16/273f, 26/273f, 16/273f, 4/273f,
              1/273f, 4/273f, 7/273f, 4/273f, 1/273f};

    protected override void applyEffect(RenderTexture src, RenderTexture dst)
    {
        if (Active)
        {
            material.SetFloatArray ("_blurArray", _blurArray);
            material.SetFloat ("_BlurOffset", Intensity);
            Graphics.Blit (src, dst, material);
        }
        else
        {
            Graphics.Blit (src, dst);
        }
    }

    void onBlurEffectValueChanged (bool value)
    {
        this.Active = value;
    }

    private void OnEnable()
    {
        Preferences.OnBlurEffectValueChanged += onBlurEffectValueChanged;
        this.Active = Preferences.BlurEffect;
    }

    private void OnDisable()
    {
        Preferences.OnBlurEffectValueChanged -= onBlurEffectValueChanged;
    }
}
