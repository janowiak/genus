﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class CutoffComponent : MonoBehaviour
{
    [SerializeField] CutoffContainer cutoffContainer;

    GameObject cutoffCopy;
    Text text;

    private void Awake()
    {
        text = GetComponent<Text> ();

        if (cutoffContainer != null)
        {
            cutoffContainer.OnCutoffContainerRefresh += onCutoffContainerRefreshed;
        }
    }

    private void OnEnable()
    {
        createCutoffCopy ();

        StartCoroutine (refreshTextComponent ());
    }

    IEnumerator refreshTextComponent ()
    {
        if (text != null && Application.isPlaying)
        {
            Text copyText = null;

            if (cutoffCopy != null)
            {
                copyText = cutoffCopy.GetComponent<Text> ();
            }

            text.enabled = false;

            if (copyText != null)
            {
                copyText.enabled = false;
            }

            yield return new WaitForEndOfFrame ();

            text.enabled = false;

            if (copyText != null)
            {
                copyText.enabled = true;
            }
        }
    }

    private void OnDisable()
    {
        if (cutoffCopy != null)
        {
            GameObject tmp = cutoffCopy;
            DestroyImmediate (tmp);
            cutoffCopy = null;
        }
    }

    void onCutoffContainerRefreshed (CutoffContainer cutoffContainer)
    {
        if (this.gameObject.activeInHierarchy)
        {
            createCutoffCopy ();
        }
    }

    void createCutoffCopy ()
    {
        destroyCopy ();

        cutoffCopy = new GameObject ();
        cutoffCopy.name = this.name + " Cutoff Copy";
        cutoffCopy.transform.SetParent (cutoffContainer.RectTransform);

        cutoffCopy.AddComponent<RectTransform> ();

        if (text != null)
        {
            cutoffCopy.AddComponent<Text> (text);
        }

        updateCopyTransform ();
    }

    void OnRectTransformDimensionsChange()
    {
        updateCopyTransform ();
    }

    private void Update()
    {
        if (transform.hasChanged)
        {
            transform.hasChanged = false;
            updateCopyTransform ();
        }
    }

    void updateCopyTransform ()
    {
        if (cutoffCopy != null)
        {
            RectTransform copyRectTransform = (RectTransform)cutoffCopy.transform;
            RectTransform originalRectTransform = (RectTransform)this.transform;

            copyRectTransform.localScale = originalRectTransform.localScale;
            copyRectTransform.localRotation = originalRectTransform.localRotation;
            copyRectTransform.rect.Set (originalRectTransform.rect.x, originalRectTransform.rect.x, originalRectTransform.rect.width, originalRectTransform.rect.height);
            copyRectTransform.position = originalRectTransform.position;
            copyRectTransform.sizeDelta = new Vector2 (originalRectTransform.rect.width, originalRectTransform.rect.height);
        }
    }

    void destroyCopy ()
    {
        if (cutoffCopy != null)
        {
            GameObject tmp = cutoffCopy;

            if (Application.isEditor)
            {
                DestroyImmediate (tmp);
            }
            else
            {
                Destroy (tmp);
            }
            
            cutoffCopy = null;
        }
    }
}
