﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

#if UNITY_EDITOR
using UnityEditor;
#endif

#if UNITY_EDITOR
[CustomEditor (typeof (ImageWithHole))]
public class UIButtonEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI ();
        ImageWithHole t = (ImageWithHole)target;
    }
}
#endif

public class ImageWithHole : Image
{
    public Sprite secondSprite;

    protected override void UpdateMaterial()
    {
        base.UpdateMaterial ();

        if (IsActive () && materialForRendering != null)
        {
            if (secondSprite != null)
            {
                materialForRendering.SetTexture ("_SecondTexture", secondSprite.texture);
            }
            else
            {
                materialForRendering.SetTexture ("_SecondTexture", Texture2D.blackTexture);
            }
        }
    }
}
