﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MainMenuWindow
{
    [SerializeField] Button selectStageButton;
    [SerializeField] MainMenuWindow selectStageWindow;

    protected override void Awake()
    {
        base.Awake ();

        if (selectStageButton != null)
        {
            selectStageButton.onClick.AddListener (() => onSelectStageButtonClicked ());
        }
    }

    void onSelectStageButtonClicked ()
    {
        switchToWindow (selectStageWindow);
    }

    protected override void onBackButtonClicked()
    {
        //DO NOTHING
    }
}
