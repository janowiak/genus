﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Logic;
using UnityEngine.UI;

public class SelectStageWindow : MainMenuWindow
{
    [SerializeField] RectTransform stagesContainer;
    [SerializeField] GameObject stageButtonPrefab;
    [SerializeField] MainMenuWindow gameplayWindow;
    [SerializeField] MainMenuWindow mainMenuWindow;
    [SerializeField] GameplayController gameplayController;
    [SerializeField] Text totalStarsGained;

    List<StageButton> stageButtons;

    protected override void Awake()
    {
        base.Awake ();

        createStageButtons ();
    }

    private void OnEnable()
    {
        refreshStarts ();
    }

    void createStageButtons ()
    {
        if (stageButtons == null)
        {
            stageButtons = new List<StageButton> ();
        }

        for (int i = 0; i < stageButtons.Count; i ++)
        {
            Destroy (stageButtons [i].gameObject);
        }

        stageButtons.Clear ();

        for (int i = 1; i < StageLoader.StageCount; i ++)
        {
            addNewStageButton (i);
        }

        refreshStarts ();
    }

    void refreshStarts ()
    {
        int totalStars = StageLoader.Instance.GetTotalStarsGained ();

        if (stageButtons != null)
        {
            for (int i = 0; i < stageButtons.Count; i ++)
            {
                if (totalStars >= StageLoader.Instance.GetStarsToUnlock (i + 1))
                {
                    stageButtons [i].SetUnlocked ();
                    stageButtons [i].RefreshStars (StageLoader.Instance.GetGainedStarsForStage (i + 1), StageLoader.Instance.GetMaxStarsForStage (i + 1));
                }
                else
                {
                    stageButtons [i].SetStarsToUnlock (StageLoader.Instance.GetStarsToUnlock (i + 1));
                    stageButtons [i].SetLocked ();
                }
            }
        }

        if (totalStarsGained != null)
        {
            totalStarsGained.text = StageLoader.Instance.GetTotalStarsGained ().ToString ();
        }
    }

    void addNewStageButton (int stageId)
    {
        if (stageButtonPrefab != null && stagesContainer != null)
        {
            GameObject newButtonObject = Instantiate (stageButtonPrefab);
            StageButton newStageButton = newButtonObject.GetComponentInChildren<StageButton> ();

            if (newStageButton != null)
            {
                if (stageButtons == null)
                {
                    stageButtons = new List<StageButton> ();
                }

                newStageButton.Init (stageId, onStageButtonClicked);
                stageButtons.Add (newStageButton);
                newButtonObject.transform.SetParent (stagesContainer, false);
            }
        }
    }

    void onStageButtonClicked (StageButton stageButton, int stageId)
    {
        int totalStarsGained = StageLoader.Instance.GetTotalStarsGained ();
        int starsToUnlock = StageLoader.Instance.GetStarsToUnlock (stageId);

        if (totalStarsGained >= starsToUnlock)
        {
            bool result = false;

            if (gameplayController != null)
            {
                result = gameplayController.Init (stageId);
            }

            if (gameplayWindow != null && result)
            {
                switchToWindow (gameplayWindow);
            }
        }
        else
        {
            string localizedMessage = Localizer.GetLocalized ("StageNotYetUnlockedKey");
            localizedMessage = string.Format (localizedMessage, starsToUnlock, totalStarsGained);
            PopupWindow.Instance.Show (localizedMessage, PopupWindow.PopupMode.OK, (RectTransform) stageButton.gameObject.transform);
        }
    }

    protected override void onBackButtonClicked()
    {
        switchToWindow (mainMenuWindow);
    }
}
