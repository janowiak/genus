﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (Collider))]
public class SceneButton : MonoBehaviour
{
    [SerializeField] bool interactible = true;
    [SerializeField] bool onHoldActive = true;
    [SerializeField] float holdThreshold = 0.8f;

    public delegate void SceneButtonEventHandler(SceneButton sceneButton);
    public event SceneButtonEventHandler OnClick;
    public event SceneButtonEventHandler OnHold;
    public event SceneButtonEventHandler OnPointerDown;

    float holdValue = 0;

    static float holdStep = 0.1f;

    public bool MouseOver
    {
        get;
        private set;
    }

    public bool MouseDown
    {
        get;
        private set;
    }

    private void OnMouseDown ()
    {
        if (interactible)
        {
            MouseDown = true;

            if (onHoldActive)
            {
                startHold ();
            }

            if (OnPointerDown != null)
            {
                OnPointerDown (this);
            }
        }
    }

    private void OnMouseEnter ()
    {
        if (interactible)
        {
            MouseOver = true;
        }
    }

    private void OnMouseExit ()
    {
        if (interactible)
        {
            MouseOver = false;
            MouseDown = false;
        }
    }

    private void OnMouseUp ()
    {
        if (interactible)
        {
            if (MouseDown)
            {
                onClick ();
            }

            MouseDown = false;
        }
    }

    IEnumerator holdEnumerator ()
    {
        while (MouseDown && holdValue < holdThreshold)
        {
            holdValue += holdStep;
            yield return new WaitForSeconds (holdStep);
        }
        
        if (MouseDown && holdValue >= holdThreshold)
        {
            onHoldEvent ();
        }

        holdValue = 0;
    }

    void startHold ()
    {
        holdValue = 0;
        StartCoroutine (holdEnumerator ());
    }

    void onClick ()
    {
        if (OnClick != null)
        {
            OnClick (this);
        }
    }

    void onHoldEvent ()
    {
        if (OnHold != null)
        {
            OnHold (this);
        }
    }
}
