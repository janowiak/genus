﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindowSwitcher : MonoBehaviour
{
    [SerializeField] BlurEffect blurEffect;
    [SerializeField] TweenerComponent cameraTweener;
    [SerializeField] MainMenuWindow optionsWindow;

    [Range (0, 10)]
    [SerializeField] float blurIntensity = 16f;

    TweenerComponent tweener;

    const float animationDuration = 0.8f;

	void Start () 
	{
		tweener = GetComponent <TweenerComponent> ();

        if (tweener == null)
        {
            tweener = this.gameObject.AddComponent<TweenerComponent> ();
        }
	}
	
	public void SwitchWindow (MainMenuWindow from, MainMenuWindow to, bool animating = true)
	{
        if (from != null && to != null && tweener != null)
        {
            from.Close ();
            to.Open ();

            if (from != optionsWindow)
            {
                to.SetPreviousWindow (from);
            }
            
            if (to == optionsWindow)
            {
                OptionsWindow ow = (OptionsWindow) optionsWindow;

                if (ow != null)
                {
                    switch (from.WindowMenuSource)
                    {
                        case MainMenuWindow.MenuSource.GAMEPLAY:

                            ow.SetBackButtonRotation (0);

                        break;

                        case MainMenuWindow.MenuSource.MAIN_MENU:

                            ow.SetBackButtonRotation (90);

                            break;
                    }
                }
            }

            tweener.TweeenValue (0f, blurIntensity, (animationDuration / 2f), onBlurUpdate, Tweener.InterpolationType.EASE_IN);
            tweener.TweeenValue (blurIntensity, 0f, (animationDuration / 2f), onBlurUpdate, Tweener.InterpolationType.EASE_OUT, (animationDuration / 2f));

            if (animating)
            {
                tweener.AnimatePosition (new Vector3 (0, 0, -to.Depth), animationDuration, true, Tweener.InterpolationType.EASE_IN_OUT);
            }
            else
            {
                this.transform.localPosition = new Vector3 (0, 0, -to.Depth);
            }
        }
    }

    void onBlurUpdate (Tweener tweener, float value)
    {
        if (blurEffect != null)
        {
            blurEffect.Intensity = value;
        }
    }
}
