﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// View of edge connecting two node views.
/// </summary>
public class EdgeView : MonoBehaviour
{
    [SerializeField] LineRenderer lineRenderer;

    const float WIDTH_SCALE = 0.5f;

    Transform node1;
    Transform node2;
    float defaultLength;

    public void Update()
    {
        if (node1 != null && node2 != null)
        {
            lineRenderer.SetPosition (0, node1.position);
            lineRenderer.SetPosition (1, node2.position);

            float d = Vector3.Distance (node1.position, node2.position);
            float width = (defaultLength / d ) * WIDTH_SCALE;
            lineRenderer.startWidth = width;
            lineRenderer.endWidth = width;
        }
    }

    public void SetNodes (Transform node1, Transform node2)
    {
        this.node1 = node1;
        this.node2 = node2;

        defaultLength = Vector3.Distance (node1.position, node2.position);
    }

    /// <summary>
    /// Check if given node transform is one of two
    /// nodes tha this edge is attached to.
    /// </summary>
    /// <param name="node"></param>
    /// <returns></returns>
    public bool IsTrasformAttached (Transform node)
    {
        bool result = false;

        if (node == node1 || node == node2)
        {
            result = true;
        }

        return result;
    }
}
