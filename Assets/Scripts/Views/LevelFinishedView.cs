﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelFinishedView : MonoBehaviour
{
    [SerializeField] SLayout sLayout;
    [SerializeField] TweenerComponent tweener;
    [SerializeField] RectTransform rectTransform;

    const float animationDuration = 0.3f;
    const float maxZ = 20000f;

    public bool Visible
    {
        get;
        private set;
    }

    public void Show (bool animating = true)
    {
        this.gameObject.SetActive (true);
        sLayout.CancelAnimations ();
        tweener.CancelAll ();
        Visible = true;

        if (animating)
        {
            sLayout.Animate (animationDuration, () =>
            {
                sLayout.groupAlpha = 1f;
            }, onShowFinished);

            tweener.AnimatePosition (new Vector3 (0, 0, 0), animationDuration * 0.9f, true, Tweener.InterpolationType.EASE_IN_OUT);
        }
        else
        {
            onShowFinished ();
        }
    }

    void onShowFinished ()
    {
        sLayout.groupAlpha = 1f;
        this.transform.localPosition = new Vector3 (0, 0, 0);
    }

    public void Hide (bool animating = true)
    {
        this.gameObject.SetActive (true);
        sLayout.CancelAnimations ();
        tweener.CancelAll ();
        Visible = true;

        if (animating)
        {
            sLayout.Animate (animationDuration, () =>
            {
                sLayout.groupAlpha = 0f;
            }, onHideFinished);

            tweener.AnimatePosition (new Vector3 (0, 0, maxZ), animationDuration * 0.9f, true, Tweener.InterpolationType.EASE_IN_OUT);
        }
        else
        {
            onHideFinished ();
        }
    }

    void onHideFinished ()
    {
        sLayout.groupAlpha = 0f;
        this.transform.localPosition = new Vector3 (0, 0, maxZ);
        this.gameObject.SetActive (false);
    }
}
