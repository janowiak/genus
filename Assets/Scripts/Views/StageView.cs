﻿using Logic;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

public class StageView : MonoBehaviour
{
    public delegate void StageViewEventHandler();
    public event StageViewEventHandler RequestRefresh;

    [SerializeField] GameObject nodeViewPrefab;
    [SerializeField] GameObject edgeViewPrefab;
    [SerializeField] GameObject anchor;
    [SerializeField] Transform stageContent;

    [SerializeField] ParticleManager particleManager;

    public delegate void StageViewNodeEventHandler(NodeView nodeView, int nodeIndex);
    public event StageViewNodeEventHandler OnNodeViewClicked;
    public event StageViewNodeEventHandler OnNodeViewPointerDown;

    List<NodeView> nodeViews;
    List<EdgeView> edgeViews;

    public TweenerComponent StageContent
    {
        get { return stageContent.gameObject.GetComponent <TweenerComponent> (); }
    }

    private void Awake()
    {
        if (particleManager != null)
        {
            particleManager.OnAllTrailParticlesFinished += onAllTrailParticlesFinished;
        }
    }

    void onAllTrailParticlesFinished ()
    {
        if (RequestRefresh != null)
        {
            RequestRefresh ();
        }
    }

    void NodeView_OnClick(NodeView nodeView)
    {
        if (OnNodeViewClicked != null)
        {
            OnNodeViewClicked (nodeView, getNodeIndex (nodeView));
        }
    }

    void NodeView_OnPoinerDown (NodeView nodeView)
    {
        if (OnNodeViewPointerDown != null)
        {
            OnNodeViewPointerDown (nodeView, getNodeIndex (nodeView));
        }
    }

    public void SendParticle (int from, int to)
    {
        if (particleManager != null)
        {
            NodeView fromNode = nodeViews [from];
            NodeView toNode = nodeViews [to];

            particleManager.RunNewParticle (fromNode, toNode);
        }
    }

    public void RefreshNodeViews (ReadOnlyCollection <Node> nodes)
    {
        if (nodes != null && nodeViews != null && nodes.Count == nodeViews.Count)
        {
            for (int i = 0; i < nodes.Count; i ++)
            {
                nodeViews [i].SetNumber (nodes [i].Value);
            }
        }
    }

    public void InitStage (StageData stageData)
    {
        deleteAll ();
        createNodes (stageData);
        createEdges (stageData);
        attachSpringJoints (stageData);
    }

    void deleteAll ()
    {
        if (nodeViews != null)
        {
            for (int i = nodeViews.Count - 1; i >= 0; i --)
            {
                Destroy (nodeViews [i].gameObject);
            }

            nodeViews.Clear ();
        }

        if (edgeViews != null)
        {
            for (int i = edgeViews.Count - 1; i >= 0; i --)
            {
                Destroy (edgeViews [i].gameObject);
            }

            edgeViews.Clear ();
        }
    }

    void createNodes (StageData stageData)
    {
        nodeViews = new List<NodeView> ();
        NodeView nodeView;

        if (stageData != null && stageData.SimpleNodes != null)
        {
            for (int i = 0; i < stageData.SimpleNodes.Count; i++)
            {
                nodeView = Instantiate (nodeViewPrefab).GetComponent<NodeView> ();
                nodeView.transform.SetParent (stageContent, false);
                nodeView.transform.localPosition = stageData.NodePositions [i].position;
                nodeView.SetNumber (stageData.SimpleNodes [i].value);
                nodeView.OnClick += NodeView_OnClick;
                nodeView.OnPointerDown += NodeView_OnPoinerDown;

                nodeViews.Add (nodeView);
            }
        }
    }

    void createEdges (StageData stageData)
    {
        if (nodeViews != null)
        {
            edgeViews = new List<EdgeView> ();
            NodeView nodeView;
            NodeView neighbourNodeView;
            EdgeView edgeView;
            bool edgeAlreadyExists = false;

            if (stageData != null && stageData.SimpleNodes != null)
            {
                for (int i = 0; i < stageData.SimpleNodes.Count; i++)
                {
                    nodeView = nodeViews [i];

                    for (int j = 0; j < stageData.SimpleNodes [i].neighbourIndices.Count; j++)
                    {
                        neighbourNodeView = nodeViews [stageData.SimpleNodes [i].neighbourIndices [j]];
                        edgeAlreadyExists = false;

                        for (int k = 0; k < edgeViews.Count; k++)
                        {
                            if (edgeViews [k].IsTrasformAttached (nodeView.transform)
                                && edgeViews [k].IsTrasformAttached (neighbourNodeView.transform))
                            {
                                edgeAlreadyExists = true;

                                break;
                            }
                        }

                        if (!edgeAlreadyExists)
                        {
                            edgeView = Instantiate (edgeViewPrefab).GetComponent<EdgeView> ();
                            edgeView.transform.SetParent (stageContent, false);
                            edgeView.SetNodes (nodeView.transform, neighbourNodeView.transform);

                            edgeViews.Add (edgeView);
                        }
                    }
                }
            }
        }
    }

    void attachSpringJoints (StageData stageData)
    {
        if (nodeViews != null)
        {
            NodeView nodeView;
            NodeView neighbourNodeView;
            SpringJoint springJoint;
            SpringJoint [] springJoints;
            bool springConnectionAlreadyExists = false;

            for (int i = 0; i < nodeViews.Count; i++)
            {
                nodeView = nodeViews [i];
                springJoint = anchor.AddComponent<SpringJoint> ();
                setNodeToAnchorSpringJoint (springJoint);
                springJoint.connectedBody = nodeView.GetComponent<Rigidbody> ();

                for (int j =0; j < stageData.SimpleNodes [i].neighbourIndices.Count; j++)
                {
                    neighbourNodeView = nodeViews [stageData.SimpleNodes [i].neighbourIndices [j]];
                    springConnectionAlreadyExists = false;
                    springJoints = neighbourNodeView.GetComponents<SpringJoint> ();

                    for (int k = 0; k < springJoints.Length; k++)
                    {
                        if (springJoints [k].connectedBody == nodeView.GetComponent <Rigidbody> ())
                        {
                            springConnectionAlreadyExists = true;

                            break;
                        }
                    }

                    if (! springConnectionAlreadyExists)
                    {
                        springJoint = nodeView.gameObject.AddComponent<SpringJoint> ();
                        setNodeToNodeSpringJoint (springJoint);
                        springJoint.connectedBody = neighbourNodeView.GetComponent<Rigidbody> ();
                    }
                }
            }
        }
    }

    void setNodeToAnchorSpringJoint (SpringJoint springJoint)
    {
        springJoint.spring = 10;
        springJoint.damper = 1;
    }

    void setNodeToNodeSpringJoint(SpringJoint springJoint)
    {
        springJoint.spring = 40;
        springJoint.damper = 5;
    }

    int getNodeIndex (NodeView nodeView)
    {
        int result = GameModel.INVALID_ID;

        if (nodeViews != null)
        {
            for (int i = 0; i < nodeViews.Count; i ++)
            {
                if (nodeViews [i] == nodeView)
                {
                    result = i;

                    break;
                }
            }
        }

        return result;
    }
}
