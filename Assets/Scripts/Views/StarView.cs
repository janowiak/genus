﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StarView : MonoBehaviour
{
    [SerializeField] Image starToGain;
    [SerializeField] Image starGained;

    public void SetGained (bool gained)
    {
        if (starGained != null && starToGain != null)
        {
            ImageProcessing.SetAlpha (starToGain, gained ? 0f : 1f);
            ImageProcessing.SetAlpha (starGained, gained ? 1f : 0f);
        }
    }
}
