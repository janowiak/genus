﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class OptionsWindow : MainMenuWindow
{
    [SerializeField] GameObject languageTogglePrefab;
    [SerializeField] RectTransform languageTogglesContainer;
    [SerializeField] CutoffContainer cutoffContainer;

    [SerializeField] Slider soundVolumeSlider;
    [SerializeField] Toggle sixtyFPSToggle;
    [SerializeField] Toggle blurEffectToggle;
    [SerializeField] Image backButtonIcon;
    [SerializeField] Button resetProgressButton;

    List<LanguageToggle> languageToggles = new List<LanguageToggle> ();

    const string areYouSureKey = "AreYouSureKey";

    protected override void Awake()
    {
        base.Awake ();

        if (resetProgressButton != null)
        {
            resetProgressButton.onClick.AddListener (() => onResetProgressButtonClicked ());
        }

        initLanguageToggles ();
    }

    private void OnEnable()
    {
        refreshToggles ();
    }

    void onResetProgressButtonClicked ()
    {
        PopupWindow.Instance.Show (Localizer.GetLocalized (areYouSureKey), PopupWindow.PopupMode.CANCEL | PopupWindow.PopupMode.OK, 
            (RectTransform) resetProgressButton.gameObject.transform, onResetProgressPopupClosed);
    }

    void onResetProgressPopupClosed (bool confirmed)
    {
        if (confirmed)
        {
            Preferences.ClearSavedGame ();
        }
    }

    void refreshToggles ()
    {
        if (soundVolumeSlider != null)
        {
            soundVolumeSlider.value = Preferences.SoundVolume;
        }

        if (sixtyFPSToggle != null)
        {
            sixtyFPSToggle.isOn = Preferences.SixtyFPS;
        }

        if (blurEffectToggle != null)
        {
            blurEffectToggle.isOn = Preferences.BlurEffect;
        }

        if (languageToggles != null)
        {
            Language currentLanguage = Preferences.Language;

            for (int i = 0; i < languageToggles.Count; i ++)
            {
                if (currentLanguage == languageToggles [i].Language)
                {
                    languageToggles [i].IsOn = true;

                    break;
                }
            }
        }
    }

    void initLanguageToggles ()
    {
        List <Language> availableLanguages = Enum.GetValues (typeof (Language)).Cast<Language> ().ToList ();
        availableLanguages.Remove (Language.NOT_DEFINED);

        if (languageToggles == null)
        {
            languageToggles = new List<LanguageToggle> ();
        }

        for (int i = 0; i < availableLanguages.Count; i ++)
        {
            if (i < languageToggles.Count)
            {
                languageToggles [i].Setup (availableLanguages [i]);
            }
            else
            {
                GameObject newLanguageToggleObject = Instantiate (languageTogglePrefab);
                newLanguageToggleObject.transform.SetParent (languageTogglesContainer, false);
                LanguageToggle languageToggle = newLanguageToggleObject.GetComponentInChildren<LanguageToggle> ();
                newLanguageToggleObject.SetActive (true);
                languageToggles.Add (languageToggle);
                languageToggle.Setup (availableLanguages [i]);
            }
        }

        for (int i = languageToggles.Count - 1; i >= availableLanguages.Count; i --)
        {
            GameObject tmp = languageToggles [i].gameObject;
            languageToggles.RemoveAt (i);
            Destroy (tmp);
        }

        Language currentLanguage = Preferences.Language;

        for (int i = 0; i < languageToggles.Count; i ++)
        {
            if (currentLanguage == languageToggles [i].Language)
            {
                languageToggles [i].Select ();

                break;
            }
        }

        StartCoroutine (forceRebuildLanguageToggles ());
    }

    IEnumerator forceRebuildLanguageToggles ()
    {
        yield return new WaitForEndOfFrame ();
        LayoutRebuilder.MarkLayoutForRebuild (languageTogglesContainer);
    }

    protected override void onBackButtonClicked()
    {
        saveValues ();

        if (cutoffContainer != null)
        {
            cutoffContainer.Refresh ();
        }

        base.onBackButtonClicked ();
    }

    void saveValues ()
    {
        float soundVolume = Preferences.SoundVolume;
        bool sixtyFPSMode = Preferences.SixtyFPS;
        bool blurEffect = Preferences.BlurEffect;
        Language language = getCurrentlySelectedLanguage ();

        if (soundVolumeSlider != null)
        {
            soundVolume = soundVolumeSlider.value;
        }

        if (sixtyFPSToggle != null)
        {
            sixtyFPSMode = sixtyFPSToggle.isOn;
        }

        if (blurEffectToggle != null)
        {
            blurEffect = blurEffectToggle.isOn;
        }

        Preferences.SaveValues (soundVolume, sixtyFPSMode, blurEffect, language);
    }

    Language getCurrentlySelectedLanguage ()
    {
        Language result = Language.ENGLISH;

        if (languageToggles != null)
        {
            for (int i = 0; i < languageToggles.Count; i ++)
            {
                if (languageToggles [i].IsOn)
                {
                    result = languageToggles [i].Language;

                    break;
                }
            }
        }

        return result;
    }

    public void SetBackButtonRotation (float rotation)
    {
        if (backButton != null)
        {
            RectTransform rectTransform = (RectTransform)backButtonIcon.transform;
            Quaternion q = rectTransform.localRotation;
            q.eulerAngles = new Vector3 (q.eulerAngles.x, q.eulerAngles.y, rotation);
            rectTransform.localRotation = q;
        }
    }
}
