﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Script attached to background image to make it follow camera and to
/// make it look like it scrolling.
/// </summary>
[ExecuteInEditMode]
public class Background : MonoBehaviour
{
    [SerializeField] Camera backgroundCamera;
    [SerializeField] float minCameraAngle = 0f;
    [SerializeField] float maxCameraAngle = 45f;
    [SerializeField] float minPosition = -420f;
    [SerializeField] float maxPosition = 420f;

    float cameraAngle;
    float normalizedAngle;
    float newX;

    void Update ()
    {
		if (backgroundCamera != null)
        {
            cameraAngle = backgroundCamera.transform.rotation.eulerAngles.y;
            normalizedAngle = (cameraAngle - minCameraAngle) / (maxCameraAngle - minCameraAngle);
            newX = maxPosition - minPosition;
            newX *= normalizedAngle;
            newX = minPosition + newX;
            this.transform.localPosition = new Vector3 (newX, this.transform.localPosition.y);
        }
	}
}
