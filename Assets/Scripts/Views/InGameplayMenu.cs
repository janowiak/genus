﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InGameplayMenu : MainMenuWindow
{
    public delegate void InGameplayMenuButtonClickedEventHandler ();
    public event InGameplayMenuButtonClickedEventHandler OnRestartStageButtonClicked;
    public event InGameplayMenuButtonClickedEventHandler OnNextStageButtonClicked;

    [SerializeField] Button nextStageButton;
    [SerializeField] Button stageListButton;
    [SerializeField] Button restartStageButton;
    [SerializeField] LevelFinishedView levelFinishedView;
    [SerializeField] Button infoButton;
    [SerializeField] GlowAnimation infoButtonGlow;

    [SerializeField] MainMenuWindow stageListWindow;

    const string tutorialMessageKey = "TutorialMessageKey";

    protected override void Awake()
    {
        base.Awake ();

        if (restartStageButton != null)
        {
            restartStageButton.onClick.AddListener (() => onRestartButtonClicked ());
        }

        if (nextStageButton != null)
        {
            nextStageButton.onClick.AddListener (() => onNextStageButtonClicked ());
        }

        if (infoButton != null)
        {
            infoButton.onClick.AddListener (() => onInfoButtonClicked ());
        }
    }

    void onNextStageButtonClicked ()
    {
        if (OnNextStageButtonClicked != null)
        {
            OnNextStageButtonClicked ();
        }
    }

    protected override void onBackButtonClicked()
    {
        switchToWindow (stageListWindow);
    }

    void onRestartButtonClicked ()
    {
        if (OnRestartStageButtonClicked != null)
        {
            OnRestartStageButtonClicked ();
        }
    }

    void onInfoButtonClicked ()
    {
        PopupWindow.Instance.Show (Localizer.GetLocalized (tutorialMessageKey), PopupWindow.PopupMode.OK, (RectTransform) infoButton.transform);
    }

    public void SetRestartButtonInteractable (bool interactable)
    {
        if (restartStageButton != null)
        {
            restartStageButton.interactable = interactable;
        }
    }

    public void SetInfoButtonGlowActive (bool active)
    {
        if (infoButtonGlow != null)
        {
            if (active)
            {
                infoButtonGlow.StartAnimation ();
            }
            else
            {
                infoButtonGlow.StopAnimating ();
            }
        }
    }

    public void SetLevelFinishedViewVisible(bool visible, bool animating = true)
    {
        if (levelFinishedView != null)
        {
            if (visible)
            {
                levelFinishedView.Show (animating);
            }
            else
            {
                levelFinishedView.Hide (animating);
            }
        }
    }

    public void SetNextStageButtonVisible (bool visible, bool animating = true)
    {
        if (nextStageButton != null)
        {
            SLayout nextStageButtonSLayout = nextStageButton.gameObject.GetComponent<SLayout> ();

            if (nextStageButtonSLayout != null)
            {
                float targetAlpha = visible ? 1f : 0f;
                nextStageButtonSLayout.CancelAnimations ();

                if (animating)
                {
                    if (visible)
                    {
                        nextStageButton.gameObject.SetActive (true);
                    }

                    if (nextStageButton.gameObject.activeSelf)
                    {
                        nextStageButtonSLayout.Animate (0.25f, () =>
                        {
                            nextStageButtonSLayout.groupAlpha = targetAlpha;
                        },
                        () =>
                        {
                            nextStageButton.gameObject.SetActive (visible);
                        });
                    }
                }
                else
                {
                    nextStageButtonSLayout.groupAlpha = targetAlpha;
                    nextStageButton.gameObject.SetActive (visible);
                }
            }
        }
        
    }
}
