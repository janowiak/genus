﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.UI;

public class LanguageToggle : MonoBehaviour
{
    [SerializeField] Toggle toggle;
    [SerializeField] Image image;

    public bool IsOn
    {
        get
        {
            bool result = false;

            if (toggle != null)
            {
                result = toggle.isOn;
            }

            return result;
        }

        set
        {
            if (toggle != null)
            {
                toggle.isOn = value;
            }
        }
    }

    public Language Language
    {
        get;
        private set;
    }

    public void Setup (Language language)
    {
        Language = language;
        setSprite (language);
    }

    public void Select()
    {
        if (toggle != null)
        {
            toggle.isOn = true;
        }
    }

    void setSprite (Language language)
    {
        if (image != null)
        {
            image.sprite = SpriteManager.Instance.GetLanguageSprite (language);
        }
    }
}
