﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupWindow : MonoBehaviour
{
    public delegate void PopupWindowEventHandler(bool confirmed);
    PopupWindowEventHandler onOnPopupWindowClosed;

    [System.Flags]
    public enum PopupMode
    {
        OK = 1,
        CANCEL = 2,
    }

    [SerializeField] Button backgroundButton;
    [SerializeField] Button okButton;
    [SerializeField] Button cancelButton;
    [SerializeField] Text message;

    [SerializeField] SLayout backgroundSLayout;
    [SerializeField] SLayout mainWindowSLayout;
    [SerializeField] SLayout openedPosition;
    [SerializeField] SLayout closedPosition;

    const float animationDuration = 0.32f;
    const float backgroundTargetAlpha = 0.9f;

    public static PopupWindow Instance
    {
        get;
        private set;
    }

    private void Awake()
    {
        Instance = this;

        if (cancelButton != null)
        {
            cancelButton.onClick.AddListener (() => onCancelButtonClicked ());
        }

        if (okButton != null)
        {
            okButton.onClick.AddListener (() => onOkButtonClicked ());
        }

        if (backgroundButton != null)
        {
            backgroundButton.onClick.AddListener (() => onCancelButtonClicked ());
        }

        onCloseFinished ();
    }

    void onOkButtonClicked ()
    {
        if (onOnPopupWindowClosed != null)
        {
            onOnPopupWindowClosed (true);
        }

        Close ();
    }

    void onCancelButtonClicked ()
    {
        if (onOnPopupWindowClosed != null)
        {
            onOnPopupWindowClosed (false);
        }

        Close ();
    }

    public void Show (string message, PopupMode popupMode, RectTransform origin, PopupWindowEventHandler onPopupClosed = null)
    {
        setButtonsVisible (popupMode);
        this.message.text = message;
        onOnPopupWindowClosed = onPopupClosed;
        closedPosition.gameObject.transform.position = origin.position;
        mainWindowSLayout.gameObject.transform.position = origin.position;

        showWindow ();
    }

    public void Close ()
    {
        onOnPopupWindowClosed = null;

        closeWindow ();
    }

    void setButtonsVisible (PopupMode popupMode)
    {
        cancelButton.gameObject.SetActive (false);
        okButton.gameObject.SetActive (false);

        if ((popupMode & PopupMode.OK) == PopupMode.OK)
        {
            okButton.gameObject.SetActive (true);
        }

        if ((popupMode & PopupMode.CANCEL) == PopupMode.CANCEL)
        {
            cancelButton.gameObject.SetActive (true);
        }
    }

    void showWindow ()
    {
        this.gameObject.SetActive (true);
        backgroundSLayout.alpha = 0f;
        mainWindowSLayout.scale = 0f;

        StartCoroutine (proceedShow ());
    }

    IEnumerator proceedShow ()
    {
        yield return new WaitForEndOfFrame ();

        backgroundSLayout.Animate (animationDuration, () =>
        {
            backgroundSLayout.alpha = backgroundTargetAlpha;
        });

        mainWindowSLayout.Animate (animationDuration, () =>
        {
            mainWindowSLayout.groupAlpha = 1f;
            mainWindowSLayout.scale = 1f;
            mainWindowSLayout.center = openedPosition.center;
        });
    }

    void closeWindow ()
    {
        this.gameObject.SetActive (true);
        backgroundSLayout.alpha = backgroundTargetAlpha;
        mainWindowSLayout.groupAlpha = 1f;
        mainWindowSLayout.scale = 1f;

        backgroundSLayout.Animate (animationDuration, () =>
        {
            backgroundSLayout.alpha = 0f;
        });

        mainWindowSLayout.Animate (animationDuration, () =>
        {
            mainWindowSLayout.groupAlpha = 0f;
            mainWindowSLayout.scale = 0f;
            mainWindowSLayout.center = closedPosition.center;
        },
        onCloseFinished);
    }

    void onCloseFinished ()
    {
        this.gameObject.SetActive (false);
        backgroundSLayout.alpha = 0f;
        mainWindowSLayout.groupAlpha = 0f;
        mainWindowSLayout.scale = 0f;
    }
}
