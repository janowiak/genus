﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MovesCounter : MonoBehaviour
{
    [SerializeField] Text text;
    [SerializeField] RectTransform oneStarContent;
    [SerializeField] RectTransform twoStarsContent;
    [SerializeField] RectTransform threeStarsContent;
    [SerializeField] Text oneStarCount;
    [SerializeField] Text twoStarsCount;
    [SerializeField] Text threeStarsCount;

    public void Refresh (int movesCount)
    {
        if (text != null)
        {
            text.text = movesCount.ToString ();
        }
    }

    public void Setup (List <int> starsThreshold)
    {
        if (starsThreshold != null && starsThreshold.Count > 0 && starsThreshold.Count <= 3)
        {
            if (starsThreshold.Count == 1)
            {
                oneStarContent.gameObject.SetActive (true);

                twoStarsContent.gameObject.SetActive (false);
                threeStarsContent.gameObject.SetActive (false);
            }
            else if (starsThreshold.Count == 2)
            {
                oneStarContent.gameObject.SetActive (true);

                twoStarsContent.gameObject.SetActive (true);
                twoStarsCount.text = starsThreshold [1].ToString ();

                threeStarsContent.gameObject.SetActive (false);
            }
            else if (starsThreshold.Count == 3)
            {
                oneStarContent.gameObject.SetActive (true);

                twoStarsContent.gameObject.SetActive (true);
                twoStarsCount.text = starsThreshold [1].ToString ();

                threeStarsContent.gameObject.SetActive (true);
                threeStarsCount.text = starsThreshold [2].ToString ();
            }
        }
    }
}
