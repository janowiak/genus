﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarsContentView : MonoBehaviour
{
    [SerializeField] GameObject starViewPrefab;

    List<StarView> stars = new List<StarView> ();

    public void SetMaxStarts (int starsCount)
    {
        int currentStarsCount = stars.Count;

        for (int i = currentStarsCount; i < starsCount; i ++)
        {
            GameObject newStar = Instantiate (starViewPrefab);
            newStar.transform.SetParent (this.transform, false);
            newStar.gameObject.SetActive (true);
            StarView newStarView = newStar.GetComponent<StarView> ();
            stars.Add (newStarView);
        }

        currentStarsCount = stars.Count;

        if (currentStarsCount > starsCount)
        {
            for (int i = 0; i < currentStarsCount - starsCount; i ++)
            {
                stars.RemoveAt (stars.Count - 1);
            }
        }
    }

    public void SetGainedStars (int gainedStars)
    {
        if (gainedStars > stars.Count)
        {
            SetMaxStarts (gainedStars);
        }

        for (int i = 0; i < stars.Count; i ++)
        {
            if (i < gainedStars)
            {
                stars [i].SetGained (true);
            }
            else
            {
                stars [i].SetGained (false);
            }
        }
    }
}
