﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StageButton : MonoBehaviour
{
    [SerializeField] Text text;
    [SerializeField] Button button;
    [SerializeField] StarsContentView starContent;
    [SerializeField] GameObject lockedContent;
    [SerializeField] GameObject unlockedContent;
    [SerializeField] Text starsToUnlock;
    [SerializeField] SLayout sLayout;

    public delegate void OnStageButtonClickedEventHandler(StageButton stageButton, int stageId);
    public event OnStageButtonClickedEventHandler OnStageButtonClicked;

    public SLayout SLayout
    {
        get { return sLayout; }
    }
    
    public int StageId
    {
        get;
        protected set;
    }

    private void Start()
    {
        if (button != null)
        {
            button.onClick.AddListener (() => onButtonClicked ());
        }
    }

    void onButtonClicked ()
    {
        if (OnStageButtonClicked != null)
        {
            OnStageButtonClicked (this, this.StageId);
        }
    }

    public void Init (int stageId, OnStageButtonClickedEventHandler onClickEvent)
    {
        if (text != null)
        {
            text.text = stageId.ToString ("D2");
        }

        this.StageId = stageId;
        this.OnStageButtonClicked += onClickEvent;
        this.gameObject.SetActive (true);
    }

    public void RefreshStars (int gained, int max)
    {
        if (starContent != null)
        {
            starContent.SetMaxStarts (max);
            starContent.SetGainedStars (gained);
        }
    }

    public void SetStarsToUnlock (int toUnlock)
    {
        if (starsToUnlock != null)
        {
            starsToUnlock.text = toUnlock.ToString ();
        }
    }

    public void SetLocked ()
    {
        if (lockedContent != null && unlockedContent != null)
        {
            lockedContent.SetActive (true);
            unlockedContent.SetActive (false);
        }
    }

    public void SetUnlocked()
    {
        if (lockedContent != null && unlockedContent != null)
        {
            lockedContent.SetActive (false);
            unlockedContent.SetActive (true);
        }
    }
}
