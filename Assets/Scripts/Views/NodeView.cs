﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeView : MonoBehaviour
{
    [SerializeField] TextMesh numberText;
    [SerializeField] SpriteRenderer background;
    [SerializeField] SceneButton sceneButton;
    [SerializeField] TweenerComponent positiveSprite;

    public delegate void NodeViewEventHandler (NodeView nodeView);
    public event NodeViewEventHandler OnClick;
    public event NodeViewEventHandler OnPointerDown;

    const float POSITIVE_SPRITE_ON_SCALE = 1.1f;

    private void OnEnable()
    {
        if (sceneButton != null)
        {
            sceneButton.OnClick += OnButtonClicked;
            sceneButton.OnPointerDown += OnButtonPointerDown;
        }
    }

    private void OnDisable()
    {
        if (sceneButton != null)
        {
            sceneButton.OnClick -= OnButtonClicked;
            sceneButton.OnPointerDown -= OnButtonPointerDown;
        }
    }

    public void Increase (bool animating = true)
    {
        int num = int.MaxValue;
        int.TryParse (numberText.text, out num);

        if (num != int.MaxValue)
        {
            num++;
            SetNumber (num, animating);
        }
    }

    public void Decrease(int count, bool animating = true)
    {
        int num = int.MaxValue;
        int.TryParse (numberText.text, out num);

        if (num != int.MaxValue)
        {
            num = num - count;
            SetNumber (num, animating);
        }
    }

    public void SetNumber (int number, bool animating = true)
    {
        numberText.text = number.ToString ();

        //Debug.Log ("Number: " + number + ". Animating: " + animating);

        if (number >= 0)
        {
            setPositiveState (animating);
        }
        else
        {
            setNegaticeState (animating);
        }
    }

    void OnButtonClicked (SceneButton button)
    {
        if (OnClick != null)
        {
            OnClick (this);
        }
    }

    void OnButtonPointerDown(SceneButton button)
    {
        if (OnPointerDown != null)
        {
            OnPointerDown (this);
        }
    }

    void setPositiveState (bool animating = true)
    {
        if (positiveSprite != null)
        {
            if (animating)
            {
                positiveSprite.AnimateScale (Vector3.one * POSITIVE_SPRITE_ON_SCALE, 0.2f, Tweener.InterpolationType.EASE_IN_OUT);
            }
            else
            {
                positiveSprite.gameObject.transform.localScale = Vector3.one * POSITIVE_SPRITE_ON_SCALE;
            }
        }
    }

    void setNegaticeState (bool animating = true)
    {
        if (positiveSprite != null)
        {
            if (animating)
            {
                positiveSprite.AnimateScale (Vector3.zero, 0.2f, Tweener.InterpolationType.EASE_IN_OUT);
            }
            else
            {
                positiveSprite.gameObject.transform.localScale = Vector3.zero;
            }
        }
    }
}
