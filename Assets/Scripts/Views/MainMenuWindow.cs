﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuWindow : MonoBehaviour
{
    public enum MainMenuWindowState
    {
        OPENED, CLOSED, OPENING, CLOSING
    }

    public enum MenuSource
    {
        GAMEPLAY, MAIN_MENU
    }

    [SerializeField] protected MenuSource menuSource = MenuSource.MAIN_MENU;
    [SerializeField] protected MenuController menuController;
    [SerializeField] protected MainMenuWindow optionsMenuWindow;
    [SerializeField] protected Button backButton;
    [SerializeField] protected Button optionsButton;
    [SerializeField] protected float depth;

    protected CanvasGroup canvasGroup;
    protected TweenerComponent tweener;
    protected MainMenuWindow previousWindow;
    protected MainMenuWindowState windowState = MainMenuWindowState.CLOSED;

    const float fadeAnimationDuration = 0.4f;

    public MenuSource WindowMenuSource
    {
        get { return menuSource; }
    }

    public float Depth
    {
        get { return depth; }
    }

    protected virtual void Awake()
    {
        if (backButton != null)
        {
            backButton.onClick.AddListener (() => onBackButtonClicked ());
        }

        if (optionsButton != null)
        {
            optionsButton.onClick.AddListener (() => onOptionsButtonClicked ());
        }

        canvasGroup = GetComponent<CanvasGroup> ();

        if (canvasGroup == null)
        {
            canvasGroup = this.gameObject.AddComponent<CanvasGroup> ();
        }

        tweener = GetComponent <TweenerComponent> ();

        if (tweener == null)
        {
            tweener = this.gameObject.AddComponent <TweenerComponent> ();
        }
    }

    public void SetPreviousWindow (MainMenuWindow previous)
    {
        this.previousWindow = previous;
    }

    protected virtual void onBackButtonClicked ()
    {
        if (previousWindow != null)
        {
            switchToWindow (previousWindow);
        }
    }

    protected virtual void onOptionsButtonClicked ()
    {
        if (optionsMenuWindow != null)
        {
            switchToWindow (optionsMenuWindow);
        }
    }

    protected void switchToWindow (MainMenuWindow newWindow)
    {
        if (menuController != null)
        {
            menuController.SwitchWindow (this, newWindow);
        }
    }

    public void Close (bool animating = true)
    {
        if (animating)
        {
            this.gameObject.SetActive (true);
            windowState = MainMenuWindowState.CLOSING;
            tweener.AnimateCanvasGroupAlpha (0f, fadeAnimationDuration, Tweener.InterpolationType.EASE_IN, 0, onAnimationFinished);
        }
        else
        {
            if (canvasGroup != null)
            {
                forceClose ();
            }
        }
    }

    void onAnimationFinished (Tweener tweener, float value)
    {
        switch (windowState)
        {
            case MainMenuWindowState.OPENING:
            case MainMenuWindowState.OPENED:

                forceOpen ();

                break;

            case MainMenuWindowState.CLOSING:
            case MainMenuWindowState.CLOSED:

                forceClose ();

                break;
        }
    }

    public void Open (bool animating = true)
    {
        if (animating)
        {
            if (canvasGroup != null)
            {
                canvasGroup.interactable = true;
                canvasGroup.blocksRaycasts = true;
            }

            this.gameObject.SetActive (true);
            windowState = MainMenuWindowState.OPENING;
            tweener.AnimateCanvasGroupAlpha (1f, fadeAnimationDuration, Tweener.InterpolationType.EASE_IN, 0, onAnimationFinished);
        }
        else
        {
            if (canvasGroup != null)
            {
                windowState = MainMenuWindowState.OPENED;
                canvasGroup.alpha = 1f;
                canvasGroup.interactable = true;
                canvasGroup.blocksRaycasts = true;
            }
        }
    }

    void forceOpen ()
    {
        this.gameObject.SetActive (true);
        windowState = MainMenuWindowState.OPENED;
        canvasGroup.alpha = 1f;
        canvasGroup.interactable = true;
        canvasGroup.blocksRaycasts = true;
    }

    void forceClose ()
    {
        this.gameObject.SetActive (false);
        windowState = MainMenuWindowState.CLOSED;
        canvasGroup.alpha = 0f;
        canvasGroup.interactable = false;
        canvasGroup.blocksRaycasts = false;
    }

    protected virtual void Update ()
    {
        if (windowState == MainMenuWindowState.OPENED)
        {
            if (Input.GetKeyDown (KeyCode.Escape))
            {
                onBackButtonClicked ();
            }
        }
    }
}
