﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplicationPreferences : MonoBehaviour
{
    void Awake()
    {
        init ();
    }

    void init ()
    {
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = Preferences.SixtyFPS ? 60 : 30;
        Language currentLanguage = Preferences.Language;
    }
}
