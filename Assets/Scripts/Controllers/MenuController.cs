﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuController : MonoBehaviour
{
    const float GAMEPLAY_CAMERA_ROTATION = 0f;
    const float MAIN_MENU_CAMERA_ROTATION = 45f;
    const float ANIMATION_DURATION = 0.8f;

    [SerializeField] TweenerComponent cameraTweener;
    [SerializeField] WindowSwitcher mainMenuWindowSwitcher;
    [SerializeField] WindowSwitcher gameplayMenuWindowSwitcher;

    MainMenuWindow.MenuSource currentGameState = MainMenuWindow.MenuSource.MAIN_MENU;

    public MainMenuWindow CurrentActiveWindow
    {
        get;
        protected set;
    }

    public void SwitchWindow (MainMenuWindow from, MainMenuWindow to)
    {
        if (from != null && to != null)
        {
            if (currentGameState == from.WindowMenuSource)
            {
                bool differentSources = to.WindowMenuSource != from.WindowMenuSource;

                if (differentSources)
                {
                    float rotation = to.WindowMenuSource == MainMenuWindow.MenuSource.GAMEPLAY ? GAMEPLAY_CAMERA_ROTATION : MAIN_MENU_CAMERA_ROTATION;
                    cameraTweener.AnimateRotation (new Vector3 (0, rotation, 0), ANIMATION_DURATION, true, Tweener.InterpolationType.EASE_IN_OUT);
                }

                switch (to.WindowMenuSource)
                {
                    case MainMenuWindow.MenuSource.MAIN_MENU:

                        mainMenuWindowSwitcher.SwitchWindow (from, to, ! differentSources);

                        break;

                    case MainMenuWindow.MenuSource.GAMEPLAY:

                        gameplayMenuWindowSwitcher.SwitchWindow (from, to, ! differentSources);

                        break;
                }

                CurrentActiveWindow = to;
                currentGameState = to.WindowMenuSource;
            }
        }
    }
}
