﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Controller of a pointer attached to a mouse pointer/finger. 
/// </summary>
public class PointerController : MonoBehaviour
{
    [SerializeField] SpringJoint springJoint;
    [SerializeField] Transform stageTransform;
    [SerializeField] Transform cameraTransform;

    float distance = 0;

    private void Update()
    {
        if (springJoint != null && springJoint.connectedBody != null)
        {
            this.transform.position = GetWorldPositionOnPlane (Input.mousePosition, distance);

            if (Input.GetMouseButtonUp (0))
            {
                springJoint.connectedBody = null;
            }
        }
    }

    public void SetNodeToFollow (NodeView nodeView)
    {
        if (springJoint != null && stageTransform != null && cameraTransform != null)
        {
            distance = stageTransform.position.z - cameraTransform.position.z;
            this.transform.position = GetWorldPositionOnPlane (Input.mousePosition, distance);
            springJoint.connectedBody = nodeView.GetComponent<Rigidbody> ();
        }
    }

    public Vector3 GetWorldPositionOnPlane(Vector3 screenPosition, float z)
    {
        Ray ray = Camera.main.ScreenPointToRay (screenPosition);
        Plane xy = new Plane (Vector3.forward, new Vector3 (0, 0, z));
        float distance;
        xy.Raycast (ray, out distance);
        return ray.GetPoint (distance);
    }
}
