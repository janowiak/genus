﻿using Logic;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

/// <summary>
/// Main controller of the game. Has reference to game model and all relevant views.
/// </summary>
public class GameplayController : MonoBehaviour
{
    [SerializeField] StageView stageView;
    [SerializeField] PointerController pointerController;
    [SerializeField] MovesCounter movesCounter;
    [SerializeField] InGameplayMenu inGameplayMenu;

    GameModel gameModel;

    public int CurrentStageID
    {
        get;
        private set;
    }

    private void Awake()
    {
        if (stageView != null)
        {
            stageView.RequestRefresh += onStageViewRefreshRequested;
        }
    }

    private void Start ()
    {
        Init (0);
    }

    /// <summary>
    /// Called from stage view. Refreshes all nodes and checkes whether stage is finished.
    /// </summary>
    void onStageViewRefreshRequested ()
    {
        refreshNodes ();

        if (gameModel.IsLevelFinished ())
        {
            onLevelFinished ();
        }
    }

    /// <summary>
    /// Initilize game with stage with given id.
    /// </summary>
    /// <param name="stageDataId"></param>
    /// <returns></returns>
    public bool Init (int stageDataId)
    {
        this.CurrentStageID = stageDataId;
        StageData stageData = StageLoader.Instance.GetStage (stageDataId);
        gameModel = new GameModel (stageData);
        stageView.InitStage (stageData);
        movesCounter.Refresh (0);
        movesCounter.Setup (stageData.StarThresholds);
        inGameplayMenu.SetNextStageButtonVisible (false);
        inGameplayMenu.SetLevelFinishedViewVisible (false, false);
        refreshRestartButton ();
        refreshInfoButtonGlow ();

        return (stageData != null);
    }

    private void OnEnable()
    {
        if (stageView != null)
        {
            stageView.OnNodeViewClicked += OnNodeViewClicked;
            stageView.OnNodeViewPointerDown += OnNodeViewPointerDown;
        }

        if (inGameplayMenu != null)
        {
            inGameplayMenu.OnRestartStageButtonClicked += onRestartStageRequested;
            inGameplayMenu.OnNextStageButtonClicked += onNextStageRequested;
        }
    }

    private void OnDisable()
    {
        if (stageView != null)
        {
            stageView.OnNodeViewClicked -= OnNodeViewClicked;
            stageView.OnNodeViewPointerDown -= OnNodeViewPointerDown;
        }

        if (inGameplayMenu != null)
        {
            inGameplayMenu.OnRestartStageButtonClicked -= onRestartStageRequested;
            inGameplayMenu.OnNextStageButtonClicked -= onNextStageRequested;
        }
    }

    /// <summary>
    /// Called by gameplay menu when next button is clicked. Loads and goes to next stage.
    /// </summary>
    void onNextStageRequested ()
    {
        inGameplayMenu.SetNextStageButtonVisible (false);
        inGameplayMenu.SetLevelFinishedViewVisible (false, false);
        TweenerComponent stageTweener = stageView.StageContent;
        stageTweener.AnimatePosition (new Vector3 (0, 0, -200), 0.5f, true, 
            Tweener.InterpolationType.EASE_IN, 
            (tweener, currentValue) => 
            {
                StartCoroutine (loadNextStage ());
            });
    }
    

    IEnumerator loadNextStage ()
    {
        TweenerComponent stageTweener = stageView.StageContent;
        Init (this.CurrentStageID + 1);
        stageTweener.gameObject.SetActive (false);

        yield return new WaitForEndOfFrame ();

        stageTweener.transform.localPosition = new Vector3 (0, 0, 1000f);
        stageTweener.gameObject.SetActive (true);
        refreshNodes ();
        refreshRestartButton ();
        stageTweener.AnimatePosition (new Vector3 (0, 0, 15f), 0.5f, false, Tweener.InterpolationType.EASE_OUT);
        refreshInfoButtonGlow ();
    }

    void onRestartStageRequested ()
    {
        StageData stageData = StageLoader.Instance.GetStage (CurrentStageID);
        gameModel = new GameModel (stageData);
        refreshNodes ();
        refreshRestartButton ();
        movesCounter.Refresh (0);
        inGameplayMenu.SetNextStageButtonVisible (false);
        inGameplayMenu.SetLevelFinishedViewVisible (false, true);
        refreshInfoButtonGlow ();
    }

    void OnNodeViewClicked (NodeView nodeView, int nodeIndex)
    {
        if (! gameModel.IsLevelFinished ())
        {
            List<int> recivers;
            gameModel.Send (nodeIndex, out recivers);

            for (int i = 0; i < recivers.Count; i++)
            {
                stageView.SendParticle (nodeIndex, recivers [i]);
            }

            nodeView.Decrease (recivers.Count);

            if (movesCounter != null)
            {
                movesCounter.Refresh (gameModel.MovesCount);
            }

            refreshRestartButton ();
            refreshInfoButtonGlow ();
        }
    }

    void refreshInfoButtonGlow ()
    {
        switch (CurrentStageID)
        {
            case 1:

                inGameplayMenu.SetInfoButtonGlowActive (gameModel.MovesCount >= 3);

                break;

            case 2:

                inGameplayMenu.SetInfoButtonGlowActive (gameModel.MovesCount >= 5);

                break;

            default:

                inGameplayMenu.SetInfoButtonGlowActive (false);

                break;
        }
    }

    void onLevelFinished ()
    {
        setGainedStars ();
        showNextLevelButtonIfAllowed ();
        inGameplayMenu.SetLevelFinishedViewVisible (true);
        refreshInfoButtonGlow ();
    }

    /// <summary>
    /// Set how many stars user gained in this stage after finishing it.
    /// Saves result in preferences.
    /// </summary>
    void setGainedStars ()
    {
        int movesDone = gameModel.MovesCount;
        List<int> starTresholds = StageLoader.Instance.GetStage (CurrentStageID).StarThresholds;
        int starsGained = 1;

        for (int i = 0; i < starTresholds.Count; i++)
        {
            if (movesDone <= starTresholds [i])
            {
                starsGained = i + 1;
            }
        }

        int previousScore = StageLoader.Instance.GetGainedStarsForStage (CurrentStageID);

        if (starsGained > previousScore)
        {
            StageLoader.Instance.SetGainedStarsForStageId (CurrentStageID, starsGained);
        }
    }

    /// <summary>
    /// Checks whether next stage button can be showed (when next stage exists)
    /// and shows it.
    /// </summary>
    void showNextLevelButtonIfAllowed ()
    {
        int nextLevelId = CurrentStageID + 1;

        if (nextLevelId < StageLoader.Instance.GetStagesCount ())
        {
            int starsRequired = StageLoader.Instance.GetStarsToUnlock (nextLevelId);

            if (StageLoader.Instance.GetTotalStarsGained () >= starsRequired)
            {
                inGameplayMenu.SetNextStageButtonVisible (true);
            }
        }
    }

    void OnNodeViewPointerDown (NodeView nodeView, int nodeIndex)
    {
        pointerController.SetNodeToFollow (nodeView);
    }

    void refreshNodes()
    {
        stageView.RefreshNodeViews (gameModel.Nodes);
    }

    void refreshRestartButton ()
    {
        bool interactable = (gameModel.MovesCount > 0);
        inGameplayMenu.SetRestartButtonInteractable (interactable);
    }
}
