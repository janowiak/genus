﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Logic
{
    public class StageLoader
    {
        const string stageDataFilePath = "Stage Data/";
        const string stageDataFileName = "stage_";
        public const int StageCount = 10;

        static StageLoader instance;

        List<StageData> stages;
        SaveGame savedGame;

        public static StageLoader Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new StageLoader ();
                }

                return instance;
            }
        }

        private StageLoader (){ }

        public void RefreshSavedGame ()
        {
            savedGame = Preferences.SavedGame;
        }

        public StageData GetStage (int stageId)
        {
            StageData result = null;

            if (stages == null || stages.Count == 0)
            {
                loadAllStages ();
            }

            if (stageId >= 0 && stageId < stages.Count)
            {
                result = stages [stageId];
            }

            return result;
        }

        void loadAllStages()
        {
            stages = new List<StageData> ();

            for (int i = 0; i < StageCount; i ++)
            {
                stages.Add (loadStage (i));
            }

            savedGame = Preferences.SavedGame;
        }

        public void SetGainedStarsForStageId (int stageId, int starsGained)
        {
            savedGame.SetStarsGained (stageId, starsGained);
            Preferences.SavedGame = savedGame;
        }

        public StageData loadStage(int stageId)
        {
            StageData stageData = null;
            string fileName =  Path.Combine(stageDataFilePath, stageDataFileName + stageId);
            TextAsset textAsset = Resources.Load<TextAsset> (fileName);

            if (textAsset != null)
            {
                string json = textAsset.text;
                stageData = JsonUtility.FromJson<StageData> (json);
            }
            else
            {
                DebugLogger.Log ("Błąd podczas wczytywania assetu poziomu!");
            }

            return stageData;
        }

        public static void SaveStageData (StageData stageData, int stageId)
        {
            string json = JsonUtility.ToJson (stageData);
            string fileName = Application.dataPath + "/Resources/" + stageDataFilePath + "/" + stageDataFileName + stageId + ".json";
            Debug.Log ("Save Stage Data " + fileName);

            using (FileStream fs = new FileStream (fileName, FileMode.Create))
            {
                using (StreamWriter writer = new StreamWriter (fs))
                {
                    writer.Write (json);
                    writer.Close ();
                }

                fs.Close ();
            }
        }

        public int GetMaxStarsForStage (int stageId)
        {
            int result = 0;

            if (stages != null)
            {
                if (stageId >= 0 && stageId < stages.Count)
                {
                    result = stages [stageId].StarThresholds.Count;
                }
            }

            return result;
        }

        public int GetStarsToUnlock(int stageId)
        {
            int result = 0;

            if (stages != null)
            {
                if (stageId >= 0 && stageId < stages.Count)
                {
                    result = stages [stageId].StarsToUnlock;
                }
            }

            return result;
        }

        public int GetGainedStarsForStage (int stageId)
        {
            int result = 0;
            
            if (savedGame == null)
            {
                savedGame = Preferences.SavedGame;
            }

            if (stageId >= 0 && stageId < savedGame.StarsGained.Count)
            {
                result = savedGame.StarsGained [stageId];
            }

            return result;
        }

        public int GetTotalStarsGained ()
        {
            int result = 0;

            if (savedGame == null)
            {
                savedGame = Preferences.SavedGame;
            }

            for (int i = 0; i < savedGame.StarsGained.Count; i ++)
            {
                result += savedGame.StarsGained [i];
            }

            return result;
        }

        public int GetStagesCount ()
        {
            int result = 0;

            if (stages != null)
            {
                result = stages.Count;
            }

            return result;
        }
    }
}
