﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Logic
{
    public class Node
    {
        public ReadOnlyCollection <Node> Neighbours
        {
            get;
            private set;
        }

        public int Value
        {
            get;
            protected internal set;
        }

        public Node (int value)
        {
            this.Value = value;
        }

        protected internal void AddNeighbour (Node node)
        {
            if (Neighbours == null)
            {
                Neighbours = new ReadOnlyCollection<Node> (new List <Node> ());
            }

            if (! Neighbours.Contains (node))
            {
                Neighbours = Neighbours.Add<Node> (node);
            }
            else
            {
                DebugLogger.Log (ActionStatus.NodeAlreadyAdded);
            }
        }

        protected internal void RemoveNeighbour(Node node)
        {
            if (Neighbours != null)
            {
                if (Neighbours.Contains (node))
                {
                    int index = Neighbours.IndexOf (node);
                    Neighbours = Neighbours.RemoveAt<Node> (index);
                }
                else
                {
                    DebugLogger.Log (ActionStatus.NodeDoesNotExist);
                }
            }
            else
            {
                DebugLogger.Log (ActionStatus.NullNeighbourList);
            }
           
        }

        /// <summary>
        /// Increases value of each connected node by one.
        /// Decreases value of this node by value equal to count of neighbours.
        /// </summary>
        /// <returns></returns>
        protected internal bool Send ()
        {
            bool result = false;

            if (Neighbours != null)
            {
                for (int i = 0; i < Neighbours.Count; i ++)
                {
                    Neighbours [i].recive ();
                }

                this.Value -= Neighbours.Count;
                result = true;
            }
            else
            {
                DebugLogger.Log (ActionStatus.NullNeighbourList);
            }

            return result;
        }

        /// <summary>
        /// Increases value of this node by one.
        /// </summary>
        protected void recive ()
        {
            Value++;
        }
    }
}

