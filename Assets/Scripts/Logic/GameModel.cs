﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

namespace Logic
{
    public class GameModel
    {
        public static int INVALID_ID = -1;
        int moves = 0;

        public int MovesCount
        {
            get { return moves; }
        }

        public ReadOnlyCollection <Node> Nodes
        {
            get;
            private set;
        }

        public GameModel (StageData stageData)
        {
            if (stageData != null)
            {
                loadStage (stageData);
            }
            else
            {
                DebugLogger.Log (ActionStatus.NullParam);
            }
        }

        public bool IsLevelFinished ()
        {
            bool result = false;

            if (Nodes != null)
            {
                result = true;

                for (int i = 0; i < Nodes.Count; i++)
                {
                    if (Nodes [i].Value < 0)
                    {
                        result = false;

                        break;
                    }
                }
            }
            
            return result;
        }

        public void CreateTestGame ()
        {
            List<Node> nodes = new List<Node> ();

            nodes.Add (new Node (-3));
            nodes.Add (new Node (-2));
            nodes.Add (new Node (2));
            nodes.Add (new Node (-5));

            nodes [0].AddNeighbour (nodes [2]);
            nodes [0].AddNeighbour (nodes [3]);

            nodes [1].AddNeighbour (nodes [2]);
            nodes [1].AddNeighbour (nodes [3]);

            nodes [2].AddNeighbour (nodes [1]);
            nodes [2].AddNeighbour (nodes [0]);
            nodes [2].AddNeighbour (nodes [3]);

            nodes [3].AddNeighbour (nodes [2]);
            nodes [3].AddNeighbour (nodes [1]);
            nodes [3].AddNeighbour (nodes [0]);


            this.Nodes = new ReadOnlyCollection<Node> (nodes);

            StageData stageData = NodesToStageData (this.Nodes);

            stageData.Add (new Vector3 (-2f, -4, 0), 0);
            stageData.Add (new Vector3 (-2, 4, 0), 1);
            stageData.Add (new Vector3 (2, 4, 0), 2);
            stageData.Add (new Vector3 (2, -4, 0), 3);

            stageData.StarThresholds = new List<int> () { 1, 2};

            StageLoader.SaveStageData (stageData, 4);
        }

        /// <summary>
        /// Called when user clicks on node view. Increases by one value of each nodes
        /// connected to node of given nodeIndex. Decreases value of node of given nodeIndex by
        /// value equal to count of nodes connected to that node. In reciversIdieces returns
        /// all nodes indiecies connected to node of given nodeIndex.
        /// </summary>
        /// <param name="nodeIndex"></param>
        /// <param name="reciversIndieces"></param>
        /// <returns></returns>
        public bool Send (int nodeIndex, out List <int> reciversIndieces)
        {
            bool result = false;
            reciversIndieces = new List<int> ();

            if (Nodes != null)
            {
                if (nodeIndex >= 0 && nodeIndex < Nodes.Count)
                {
                    result = Nodes [nodeIndex].Send ();
                    reciversIndieces = getNodeIndexes (Nodes [nodeIndex].Neighbours);
                    moves++;
                }
                else
                {
                    DebugLogger.Log (ActionStatus.IndexOutOfRange);
                }
            }
            else
            {
                DebugLogger.Log (ActionStatus.NullNodesList);
            }

            return result;
        }

        List <int> getNodeIndexes (ReadOnlyCollection <Node> nodesToCheck)
        {
            List<int> result = new List<int> ();

            for (int i = 0; i < nodesToCheck.Count; i ++)
            {
                result.Add (GetNodeIndex (nodesToCheck [i]));
            }

            return result;
        }

        protected internal StageData NodesToStageData (ReadOnlyCollection <Node> nodes)
        {
            StageData stageData = new StageData ();

            if (nodes != null)
            {
                SimpleNode simpleNode;
                int neighbourIndex = INVALID_ID;

                for (int i = 0; i < nodes.Count; i++)
                {
                    simpleNode = new SimpleNode (nodes [i].Value, i);

                    if (nodes [i].Neighbours != null)
                    {
                        for (int j = 0; j < nodes [i].Neighbours.Count; j++)
                        {
                            neighbourIndex = GetNodeIndex (nodes [i].Neighbours [j]);

                            if (neighbourIndex != INVALID_ID)
                            {
                                simpleNode.AddNeighbour (neighbourIndex);
                            }
                        }
                    }
                    else
                    {
                        DebugLogger.Log (ActionStatus.NullNeighbourList);
                    }

                    stageData.Add (simpleNode);
                }
            }
            else
            {
                DebugLogger.Log (ActionStatus.NullNodesList);
            }

            return stageData;
        }

        ReadOnlyCollection <Node> stageDataToNodes (StageData stageData)
        {
            ReadOnlyCollection<Node> result;
            List<Node> tmpList = new List<Node> ();

            if (stageData != null)
            {
                for (int i = 0; i < stageData.SimpleNodes.Count; i ++)
                {
                    tmpList.Add (new Node (stageData.SimpleNodes [i].value));
                }

                for (int i = 0; i < stageData.SimpleNodes.Count; i++)
                {
                    for (int j = 0; j < stageData.SimpleNodes [i].neighbourIndices.Count; j ++)
                    {
                        tmpList [i].AddNeighbour (tmpList [stageData.SimpleNodes [i].neighbourIndices [j]]);
                    }
                }
            }

            result = new ReadOnlyCollection<Node> (tmpList);
            return result;
        }

        void loadStage (StageData stageData)
        {
            this.Nodes = stageDataToNodes (stageData);
            this.moves = 0;
        }

        public int GetNodeIndex (Node node)
        {
            int result = INVALID_ID;

            if (Nodes != null)
            {
                for (int i = 0; i < Nodes.Count; i ++)
                {
                    if (node == Nodes [i])
                    {
                        result = i;

                        break;
                    }
                }
            }
            else
            {
                DebugLogger.Log (ActionStatus.NullNodesList);
            }

            return result;
        }
    }
}

