﻿Shader "Custom/Blur"
{
	Properties
	{
		_MainTex("Base (RGB)", 2D) = "white" {}
		_BlurOffset ("Blur Offset", Float) = 4
	}

		SubShader
		{
			Pass
			{

			CGPROGRAM
			uniform float _blurArray [25];
			#pragma vertex vert_img
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest
			#include "UnityCG.cginc"

			struct Input
			{
				float4 pos : SV_POSITION;
				half2 uv   : TEXCOORD0;
			};

			uniform sampler2D _MainTex;
			uniform float _BlurOffset;
			uniform float _Strength;
			float4 _MainTex_TexelSize;

			float4 frag(Input i) : COLOR
			{
				float2 coords = i.uv.xy;

				float4 colorSum;
				float index = 0;

				for (int x = -2; x <= 2; x++)
				{
					for (int y = -2; y <= 2; y++)
					{
						colorSum += tex2D(_MainTex, coords.xy + float2 (x * _MainTex_TexelSize.x * _BlurOffset, y * _MainTex_TexelSize.y * _BlurOffset)) * _blurArray [index];
						index++;
					}
				}

				return colorSum;
			}

			ENDCG
		}
		}
}