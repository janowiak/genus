﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlowAnimation : MonoBehaviour
{
    [SerializeField] SLayout sLayout;

    const float animationDuration = 0.85f;

    public void StartAnimation ()
    {
        sLayout.CancelAnimations ();
        sLayout.Animate (animationDuration, fadeIn, onFadeInComplete);
    }

    void fadeIn ()
    {
        sLayout.alpha = 1f;
    }

    void onFadeInComplete ()
    {
        sLayout.Animate (animationDuration, fadeOut, onFadeOutComplete);
    }

    void fadeOut ()
    {
        sLayout.alpha = 0f;
    }

    void onFadeOutComplete ()
    {
        sLayout.Animate (animationDuration, fadeIn, onFadeInComplete);
    }

    public void StopAnimating ()
    {
        sLayout.CancelAnimations ();
        sLayout.Animate (0.5f, () => { sLayout.alpha = 0f; });
    }
}
