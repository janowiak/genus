﻿using Logic;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

public class GameplayController : MonoBehaviour
{
    [SerializeField] StageView stageView;
    [SerializeField] PointerController pointerController;

    GameModel gameModel;

    private void Start ()
    {
        StageData stageData = StageLoader.LoadStage (0);

        gameModel = new GameModel (stageData);
        stageView.InitStage (stageData);

        //gameModel = new GameModel ();
    }

    private void OnEnable()
    {
        if (stageView != null)
        {
            stageView.OnNodeViewClicked += OnNodeViewClicked;
            stageView.OnNodeViewPointerDown += OnNodeViewPointerDown;
        }
    }

    private void OnDisable()
    {
        if (stageView != null)
        {
            stageView.OnNodeViewClicked -= OnNodeViewClicked;
            stageView.OnNodeViewPointerDown -= OnNodeViewPointerDown;
        }
    }

    void OnNodeViewClicked (NodeView nodeView, int nodeIndex)
    {
        gameModel.Send (nodeIndex);
        refreshNodes ();
    }

    void OnNodeViewPointerDown (NodeView nodeView, int nodeIndex)
    {
        pointerController.SetNodeToFollow (nodeView);
    }

    void refreshNodes()
    {
        stageView.RefreshNodeViews (gameModel.Nodes);
    }
}
