﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointerController : MonoBehaviour
{
    [SerializeField] SpringJoint springJoint;

    private void Update()
    {
        if (springJoint.connectedBody != null)
        {
            this.transform.position = GetWorldPositionOnPlane (Input.mousePosition, 0);

            if (Input.GetMouseButtonUp (0))
            {
                springJoint.connectedBody = null;
            }
        }
    }

    public void SetNodeToFollow (NodeView nodeView)
    {
        this.transform.position = Camera.main.ScreenToWorldPoint (Input.mousePosition);
        springJoint.connectedBody = nodeView.GetComponent<Rigidbody> ();
    }

    public Vector3 GetWorldPositionOnPlane(Vector3 screenPosition, float z)
    {
        Ray ray = Camera.main.ScreenPointToRay (screenPosition);
        Plane xy = new Plane (Vector3.forward, new Vector3 (0, 0, z));
        float distance;
        xy.Raycast (ray, out distance);
        return ray.GetPoint (distance);
    }

}
