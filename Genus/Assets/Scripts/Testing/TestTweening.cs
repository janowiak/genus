﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestTweening : MonoBehaviour {

    [SerializeField] MainMenuWindow w1;
    [SerializeField] MainMenuWindow w2;

    [SerializeField] WindowSwitcher windowSwitcher;

	void Start ()
    {
        StartCoroutine (delay ());
    }

    IEnumerator delay ()
    {
        yield return new WaitForSeconds (2f);

        windowSwitcher.SwitchWindow (w1, w2);
    }

}
