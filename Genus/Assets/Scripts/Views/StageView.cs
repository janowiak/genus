﻿using Logic;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

public class StageView : MonoBehaviour
{
    [SerializeField] GameObject nodeViewPrefab;
    [SerializeField] GameObject edgeViewPrefab;
    [SerializeField] GameObject anchor;

    public delegate void StageViewEventHandler(NodeView nodeView, int nodeIndex);
    public event StageViewEventHandler OnNodeViewClicked;
    public event StageViewEventHandler OnNodeViewPointerDown;

    List<NodeView> nodeViews;
    List<EdgeView> edgeViews;

    void NodeView_OnClick(NodeView nodeView)
    {
        if (OnNodeViewClicked != null)
        {
            OnNodeViewClicked (nodeView, getNodeIndex (nodeView));
        }
    }

    void NodeView_OnPoinerDown (NodeView nodeView)
    {
        if (OnNodeViewPointerDown != null)
        {
            OnNodeViewPointerDown (nodeView, getNodeIndex (nodeView));
        }
    }

    public void RefreshNodeViews (ReadOnlyCollection <Node> nodes)
    {
        if (nodes != null && nodeViews != null && nodes.Count == nodeViews.Count)
        {
            for (int i = 0; i < nodes.Count; i ++)
            {
                nodeViews [i].SetNumber (nodes [i].Value);
            }
        }
    }

    public void InitStage (StageData stageData)
    {
        createNodes (stageData);
        createEdges (stageData);
        attachSpringJoints (stageData);
    }

    void createNodes (StageData stageData)
    {
        nodeViews = new List<NodeView> ();
        NodeView nodeView;

        for (int i = 0; i < stageData.SimpleNodes.Count; i++)
        {
            nodeView = Instantiate (nodeViewPrefab).GetComponent<NodeView> ();
            nodeView.transform.SetParent (this.transform);
            nodeView.transform.localPosition = stageData.NodePositions [i].position;
            //nodeView.transform.
            nodeView.SetNumber (stageData.SimpleNodes [i].value);
            nodeView.OnClick += NodeView_OnClick;
            nodeView.OnPointerDown += NodeView_OnPoinerDown;

            nodeViews.Add (nodeView);
        }
    }

    void createEdges (StageData stageData)
    {
        if (nodeViews != null)
        {
            edgeViews = new List<EdgeView> ();
            NodeView nodeView;
            NodeView neighbourNodeView;
            EdgeView edgeView;
            bool edgeAlreadyExists = false;

            for (int i = 0; i < stageData.SimpleNodes.Count; i++)
            {
                nodeView = nodeViews [i];

                for (int j = 0; j < stageData.SimpleNodes [i].neighbourIndices.Count; j ++)
                {
                    neighbourNodeView = nodeViews [stageData.SimpleNodes [i].neighbourIndices [j]];
                    edgeAlreadyExists = false;

                    for (int k = 0; k < edgeViews.Count; k++)
                    {
                        if (edgeViews [k].IsTrasformAttached (nodeView.transform) 
                            && edgeViews [k].IsTrasformAttached (neighbourNodeView.transform))
                        {
                            edgeAlreadyExists = true;

                            break;
                        }
                    }

                    if (! edgeAlreadyExists)
                    {
                        edgeView = Instantiate (edgeViewPrefab).GetComponent<EdgeView> ();
                        edgeView.transform.SetParent (this.transform);
                        edgeView.SetNodes (nodeView.transform, neighbourNodeView.transform);

                        edgeViews.Add (edgeView);
                    }
                }
            }
        }
    }

    void attachSpringJoints (StageData stageData)
    {
        if (nodeViews != null)
        {
            NodeView nodeView;
            NodeView neighbourNodeView;
            SpringJoint springJoint;
            SpringJoint [] springJoints;
            bool springConnectionAlreadyExists = false;

            for (int i = 0; i < nodeViews.Count; i++)
            {
                nodeView = nodeViews [i];
                springJoint = anchor.AddComponent<SpringJoint> ();
                setNodeToAnchorSpringJoint (springJoint);
                springJoint.connectedBody = nodeView.GetComponent<Rigidbody> ();

                for (int j =0; j < stageData.SimpleNodes [i].neighbourIndices.Count; j++)
                {
                    neighbourNodeView = nodeViews [stageData.SimpleNodes [i].neighbourIndices [j]];
                    springConnectionAlreadyExists = false;
                    springJoints = neighbourNodeView.GetComponents<SpringJoint> ();

                    for (int k = 0; k < springJoints.Length; k++)
                    {
                        if (springJoints [k].connectedBody == nodeView.GetComponent <Rigidbody> ())
                        {
                            springConnectionAlreadyExists = true;

                            break;
                        }
                    }

                    if (! springConnectionAlreadyExists)
                    {
                        springJoint = nodeView.gameObject.AddComponent<SpringJoint> ();
                        setNodeToNodeSpringJoint (springJoint);
                        springJoint.connectedBody = neighbourNodeView.GetComponent<Rigidbody> ();
                    }
                }
            }
        }
    }

    void setNodeToAnchorSpringJoint (SpringJoint springJoint)
    {
        springJoint.spring = 10;
        springJoint.damper = 1;
    }

    void setNodeToNodeSpringJoint(SpringJoint springJoint)
    {
        springJoint.spring = 40;
        springJoint.damper = 5;
    }

    int getNodeIndex (NodeView nodeView)
    {
        int result = GameModel.INVALID_ID;

        if (nodeViews != null)
        {
            for (int i = 0; i < nodeViews.Count; i ++)
            {
                if (nodeViews [i] == nodeView)
                {
                    result = i;

                    break;
                }
            }
        }

        return result;
    }
}
