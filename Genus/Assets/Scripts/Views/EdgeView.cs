﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EdgeView : MonoBehaviour
{
    [SerializeField] LineRenderer lineRenderer;

    Transform node1;
    Transform node2;
    float defaultLength;

    public void Update()
    {
        if (node1 != null && node2 != null)
        {
            lineRenderer.SetPosition (0, node1.position);
            lineRenderer.SetPosition (1, node2.position);

            float d = Vector3.Distance (node1.position, node2.position);
            float width = defaultLength / d / 5;
            lineRenderer.SetWidth (width, width);
        }
    }

    public void SetNodes (Transform node1, Transform node2)
    {
        this.node1 = node1;
        this.node2 = node2;

        defaultLength = Vector3.Distance (node1.position, node2.position);
    }

    public bool IsTrasformAttached (Transform node)
    {
        bool result = false;

        if (node == node1 || node == node2)
        {
            result = true;
        }

        return result;
    }
}
