﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindowSwitcher : MonoBehaviour
{
    [SerializeField] PostProcessingEffect blurEffect;

    TweenerComponent tweener;

    const float animationDuration = 0.8f;

	void Start () 
	{
		tweener = GetComponent <TweenerComponent> ();
	}
	
	public void SwitchWindow (MainMenuWindow from, MainMenuWindow to)
	{
        if (from != null && to != null && tweener != null)
        {
            from.Close ();
            to.Open ();

            tweener.TweeenValue (0f, 2f, (animationDuration / 2f), onBlurUpdate, Tweener.InterpolationType.EASE_IN);
            tweener.TweeenValue (2f, 0f, (animationDuration / 2f), onBlurUpdate, Tweener.InterpolationType.EASE_OUT, (animationDuration / 2f));
            tweener.AnimatePosition (new Vector3 (0, 0, -to.Depth), animationDuration, true, Tweener.InterpolationType.EASE_IN_OUT);
        }
    }

    void onBlurUpdate (Tweener tweener, float value)
    {
        if (blurEffect != null)
        {
            blurEffect.Intensity = value;
        }
    }
}
