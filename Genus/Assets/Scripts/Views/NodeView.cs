﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeView : MonoBehaviour
{
    [SerializeField] TextMesh numberText;
    [SerializeField] SpriteRenderer background;
    [SerializeField] SceneButton sceneButton;

    public delegate void NodeViewEventHandler (NodeView nodeView);
    public event NodeViewEventHandler OnClick;
    public event NodeViewEventHandler OnPointerDown;

    private void OnEnable()
    {
        if (sceneButton != null)
        {
            sceneButton.OnClick += OnButtonClicked;
            sceneButton.OnPointerDown += OnButtonPointerDown;
        }
    }

    private void OnDisable()
    {
        if (sceneButton != null)
        {
            sceneButton.OnClick -= OnButtonClicked;
            sceneButton.OnPointerDown -= OnButtonPointerDown;
        }
    }

    public void SetNumber (int number, bool animating = true)
    {
        numberText.text = number.ToString ();
    }

    void OnButtonClicked (SceneButton button)
    {
        if (OnClick != null)
        {
            OnClick (this);
        }
    }

    void OnButtonPointerDown(SceneButton button)
    {
        if (OnPointerDown != null)
        {
            OnPointerDown (this);
        }
    }
}
