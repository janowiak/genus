﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (Collider))]
public class SceneButton : MonoBehaviour
{
    [SerializeField] bool interactible = true;
    [SerializeField] bool onHoldActive = true;
    [SerializeField] float holdThreshold = 0.8f;

    public delegate void SceneButtonEventHandler(SceneButton sceneButton);
    public event SceneButtonEventHandler OnClick;
    public event SceneButtonEventHandler OnHold;
    public event SceneButtonEventHandler OnPointerDown;

    SpriteRenderer spriteRenderer;
    bool mouseOver = false;
    bool mouseDown = false;
    float holdValue = 0;

    static float holdStep = 0.1f;

    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer> ();
    }

    private void OnMouseDown ()
    {
        if (interactible)
        {
            mouseDown = true;

            if (onHoldActive)
            {
                startHold ();
            }

            if (OnPointerDown != null)
            {
                OnPointerDown (this);
            }
        }
    }

    private void OnMouseEnter ()
    {
        if (interactible)
        {
            mouseOver = true;
        }
    }

    private void OnMouseExit ()
    {
        if (interactible)
        {
            mouseOver = false;
            mouseDown = false;
        }
    }

    private void OnMouseUp ()
    {
        if (interactible)
        {
            if (mouseDown)
            {
                onClick ();
            }

            mouseDown = false;
        }
    }

    IEnumerator holdEnumerator ()
    {
        while (mouseDown && holdValue < holdThreshold)
        {
            holdValue += holdStep;
            yield return new WaitForSeconds (holdStep);
        }
        
        if (mouseDown && holdValue >= holdThreshold)
        {
            onHoldEvent ();
        }

        holdValue = 0;
    }

    void startHold ()
    {
        holdValue = 0;
        StartCoroutine (holdEnumerator ());
    }

    void onClick ()
    {
        if (OnClick != null)
        {
            OnClick (this);
        }
    }

    void onHoldEvent ()
    {
        if (OnHold != null)
        {
            OnHold (this);
        }
    }
}
