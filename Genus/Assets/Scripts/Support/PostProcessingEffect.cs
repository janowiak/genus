﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PostProcessingEffect : MonoBehaviour {

    [SerializeField] float intensity = 1f;
    [SerializeField] float strength = 1f;
    [SerializeField] Material material;

    public float Intensity
    {
        get { return intensity; }
        set { intensity = value; }
    }

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        float [] _blurArray = new float [] {1/273f, 4/237f, 7/237f, 4/237f, 1/237f, 
			4/237f, 16/237f, 26/237f, 16/237f, 4/237f,
			 7/237f, 26/237f, 41/237f, 26/237f, 7/237f,
			  4/237f, 16/237f, 26/237f, 16/237f, 4/237f, 
			  1/237f, 4/237f, 7/237f, 4/237f, 1/237f};

        material.SetFloatArray("_blurArray", _blurArray);
        material.SetFloat ("_BlurOffset", intensity);
        material.SetFloat ("_Strength", strength);
        Graphics.Blit (source, destination, material);

        
    }
}
