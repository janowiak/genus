﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

namespace Logic
{
    public class GameModel
    {
        public static int INVALID_ID = -1;

        ReadOnlyCollection <Node> nodes;
        int moves = 0;
        int stageIndex = INVALID_ID;

        public int MovesCount
        {
            get { return moves; }
        }

        public ReadOnlyCollection <Node> Nodes
        {
            get { return nodes; }
        }

        public GameModel ()
        {
            createTestGame ();
        }

        public GameModel (StageData stageData)
        {
            if (stageData != null)
            {
                loadStage (stageData);
            }
            else
            {
                DebugLogger.Log (ActionStatus.NullParam);
            }
        }

        void createTestGame ()
        {
            List<Node> nodes = new List<Node> ();

            nodes.Add (new Node (2));
            nodes.Add (new Node (-1));
            nodes.Add (new Node (-1));

            nodes [0].AddNeighbour (nodes [1]);
            nodes [0].AddNeighbour (nodes [2]);

            nodes [1].AddNeighbour (nodes [0]);
            nodes [1].AddNeighbour (nodes [2]);

            nodes [2].AddNeighbour (nodes [1]);
            nodes [2].AddNeighbour (nodes [0]);

            this.nodes = new ReadOnlyCollection<Node> (nodes);

            StageData stageData = NodesToStageData (this.nodes);

            stageData.Add (new Vector3 (0, 20, 0), 0);
            stageData.Add (new Vector3 (-20, -20, 0), 1);
            stageData.Add (new Vector3 (20, -20, 0), 2);

            StageLoader.SaveStageData (stageData, 0);
        }

        public bool Send (int nodeIndex)
        {
            bool result = false;

            if (nodes != null)
            {
                if (nodeIndex >= 0 && nodeIndex < nodes.Count)
                {
                    result = nodes [nodeIndex].Send ();
                    moves++;
                }
                else
                {
                    DebugLogger.Log (ActionStatus.IndexOutOfRange);
                }
            }
            else
            {
                DebugLogger.Log (ActionStatus.NullNodesList);
            }

            return result;
        }

        protected internal StageData NodesToStageData (ReadOnlyCollection <Node> nodes)
        {
            StageData stageData = new StageData ();

            if (nodes != null)
            {
                SimpleNode simpleNode;
                int neighbourIndex = INVALID_ID;

                for (int i = 0; i < nodes.Count; i++)
                {
                    simpleNode = new SimpleNode (nodes [i].Value, i);

                    if (nodes [i].Neighbours != null)
                    {
                        for (int j = 0; j < nodes [i].Neighbours.Count; j++)
                        {
                            neighbourIndex = GetNodeIndex (nodes [i].Neighbours [j]);

                            if (neighbourIndex != INVALID_ID)
                            {
                                simpleNode.AddNeighbour (neighbourIndex);
                            }
                        }
                    }
                    else
                    {
                        DebugLogger.Log (ActionStatus.NullNeighbourList);
                    }

                    stageData.Add (simpleNode);
                }
            }
            else
            {
                DebugLogger.Log (ActionStatus.NullNodesList);
            }

            return stageData;
        }

        ReadOnlyCollection <Node> stageDataToNodes (StageData stageData)
        {
            ReadOnlyCollection<Node> result;
            List<Node> tmpList = new List<Node> ();

            if (stageData != null)
            {
                for (int i = 0; i < stageData.SimpleNodes.Count; i ++)
                {
                    tmpList.Add (new Node (stageData.SimpleNodes [i].value));
                }

                for (int i = 0; i < stageData.SimpleNodes.Count; i++)
                {
                    for (int j = 0; j < stageData.SimpleNodes [i].neighbourIndices.Count; j ++)
                    {
                        tmpList [i].AddNeighbour (tmpList [stageData.SimpleNodes [i].neighbourIndices [j]]);
                    }
                }
            }

            result = new ReadOnlyCollection<Node> (tmpList);
            return result;
        }

        void loadStage (StageData stageData)
        {
            this.nodes = stageDataToNodes (stageData);
            this.moves = 0;
        }

        public int GetNodeIndex (Node node)
        {
            int result = INVALID_ID;

            if (nodes != null)
            {
                for (int i = 0; i < nodes.Count; i ++)
                {
                    if (node == nodes [i])
                    {
                        result = i;

                        break;
                    }
                }
            }
            else
            {
                DebugLogger.Log (ActionStatus.NullNodesList);
            }

            return result;
        }
    }
}

