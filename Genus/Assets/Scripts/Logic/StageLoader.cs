﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Logic
{
    public static class StageLoader
    {
        const string resourcesPath = "Assets/Resources/";
        const string stageDataFilePath = "Stage Data/";
        const string stageDataFileName = "stage_";

        public static StageData LoadStage (int stageId)
        {
            StageData stageData = null;
            string fileName = stageDataFilePath + stageDataFileName + stageId;
            Debug.Log ("file name: " + fileName);
            TextAsset textAsset = Resources.Load<TextAsset> ("Stage Data/stage_0");

            if (textAsset != null)
            {
                string json = textAsset.text;
                stageData = JsonUtility.FromJson<StageData> (json);
            }
            else
            {
                DebugLogger.Log ("Błąd podczas wczytywania assetu poziomu!");
            }

            return stageData;
        }

        public static void SaveStageData (StageData stageData, int stageId)
        {
            string json = JsonUtility.ToJson (stageData);
            string fileName = resourcesPath + stageDataFilePath + stageDataFileName + stageId + ".json";

            using (FileStream fs = new FileStream (fileName, FileMode.Create))
            {
                using (StreamWriter writer = new StreamWriter (fs))
                {
                    writer.Write (json);
                }
            }
        }
    }
}
