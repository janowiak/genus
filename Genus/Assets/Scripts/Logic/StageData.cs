﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Logic
{
    [Serializable]
    public class StageData
    {
        public List<SimpleNode> SimpleNodes = new List<SimpleNode> ();
        public List<NodePosition> NodePositions = new List<NodePosition> ();

        public void Add(SimpleNode simpleNode)
        {
            this.SimpleNodes.Add (simpleNode);
        }

        public void Add (NodePosition nodePosition)
        {
            this.NodePositions.Add (nodePosition);
        }

        public void Add (Vector3 postion, int nodeIndex)
        {
            this.NodePositions.Add (new NodePosition (postion, nodeIndex));
        }

        public List <Vector3> GetOnlyPositions ()
        {
            List<Vector3> result = new List<Vector3> ();

            for (int i =0; i <NodePositions.Count; i++)
            {
                result.Add (NodePositions [i].position);
            }

            return result;
        }
    }

    [Serializable]
    public class SimpleNode
    {
        public List<int> neighbourIndices = new List<int> ();
        public int value;
        public int nodeIndex;

        public SimpleNode(int value, int nodeIndex)
        {
            this.value = value;
            this.nodeIndex = nodeIndex;
        }

        public void AddNeighbour(int neighbourIndex)
        {
            neighbourIndices.Add (neighbourIndex);
        }
    }

    [Serializable]
    public class NodePosition
    {
        public Vector3 position;
        public int nodeIndex;

        public NodePosition (Vector3 position, int nodeIndex)
        {
            this.position = position;
            this.nodeIndex = nodeIndex;
        }
    }
}

