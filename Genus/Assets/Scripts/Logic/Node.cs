﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Logic
{
    public class Node
    {
        ReadOnlyCollection<Node> neighbours;
        int value = 0;

        public ReadOnlyCollection <Node> Neighbours
        {
            get { return neighbours; }
        }

        public int Value
        {
            get { return value; }
            protected internal set { this.value = value; }
        }

        public Node (int value)
        {
            this.value = value;
        }

        protected internal void AddNeighbour (Node node)
        {
            if (neighbours == null)
            {
                neighbours = new ReadOnlyCollection<Node> (new List <Node> ());
            }

            if (! neighbours.Contains (node))
            {
                neighbours = neighbours.Add<Node> (node);
            }
            else
            {
                DebugLogger.Log (ActionStatus.NodeAlreadyAdded);
            }
        }

        protected internal void RemoveNeighbour(Node node)
        {
            if (neighbours != null)
            {
                if (neighbours.Contains (node))
                {
                    int index = neighbours.IndexOf (node);
                    neighbours = neighbours.RemoveAt<Node> (index);
                }
                else
                {
                    DebugLogger.Log (ActionStatus.NodeDoesNotExist);
                }
            }
            else
            {
                DebugLogger.Log (ActionStatus.NullNeighbourList);
            }
           
        }

        protected internal bool Send ()
        {
            bool result = false;

            if (neighbours != null)
            {
                for (int i = 0; i < neighbours.Count; i ++)
                {
                    neighbours [i].recive ();
                }

                this.value -= neighbours.Count;
                result = true;
            }
            else
            {
                DebugLogger.Log (ActionStatus.NullNeighbourList);
            }

            return result;
        }

        protected void recive ()
        {
            value++;
        }
    }

    
}

